const path = require('path');
const fs = require('fs');
const util = require('util');
const formidable = require('formidable');
const sqlite3 = require('sqlite3').verbose();
const child_process = require('child_process');
const express = require('express');
const router = express.Router();
const upDirPath = path.join(__dirname, '../../uploads');
var db_file = path.join(upDirPath, 'data.db');
var db_file_dir = null;
var results_avail = 0;
var result_data = [];
var totalTests = 0;

router.post('/', (req, res) => {
    console.log('base post route')
    res.send('This is base post route')
})

// upload data.db file
router.post('/upload', async(req, res) => {
    let date = new Date();
    let m= (date.getMonth() + 1).toString();
    let ddate = 'Data' + '_' + date.getDate() + '_' + m + '_' + date.getFullYear();
    let data_path = path.join(upDirPath, ddate);
    // update dir path
    db_file_dir = data_path;
    // create dir to store files
    await fs.mkdir(data_path, (err) => {
        if (err) {
            res.json({message: err});
            console.log(db_file_dir);
            console.log('Warning! Directory exists.')
            console.log('File is overwritten');
            upload_now(req, res, db_file_dir);
        } else {
            console.log('Created dir: ' + db_file_dir);
            db_file = path.join(db_file_dir, 'data.db');
            upload_now(req, res, db_file_dir);
        }
    })
});

function upload_now(request, response, data_dir) {
    var form = new formidable.IncomingForm({ multiples: true, uploadDir: data_dir});
    form.on('file', function(field, file) {
        fs.rename(file.path, path.join(form.uploadDir, file.name), (err) => {
            console.log('File ' + file.name + ' uploaded');
        });
    });
    form.on('end', () => {
        response.end('Successful upload!');
    })
    form.parse(request);
}

router.post('/upload_data', (req,res) => {
    upload_now(req, res, db_file_dir);
});

router.post('/addPat', (req, res) => {
    console.log('Querying database');

    // const upDirPath = path.join(__dirname, '../../uploads');
    // const db_file = path.join(__dirname, '../../uploads/data.db');

    let db = new sqlite3.Database(db_file);
    let sql_str = 'SELECT mrn, dob, isMale, testKey FROM PATIENT_RECORD PR JOIN TEST_INSTANCE_PATIENT_INFO TIPI ' +
        'ON PR.key=TIPI.patientKey';

    db.all(sql_str, [], (err, row) => {
        if (err) {
            console.log(err);
        }
        let patInfo = []
        row.forEach((row) => {
            var patData = {
                mrn: row.mrn,
                age: 2020 - row.dob,            // use date of test!!!
                isMale: row.isMale,
                testKey: row.testKey
            }
            patInfo.push(patData);
        });
        //console.log(patInfo);
        db.close();
        res.send(patInfo);
    })
});

router.post('/run_model', (req, res) => {
    console.log('Running Python code')
    const patData = req.body;
    totalTests = patData.length;

    // const db_file = '/home/sergeyt/WebProjects/uploads'
    // const dir_out = '/home/sergeyt/DataTest'

    // var spawn = child_process.spawn;
    // var process = spawn('python3', ["src/test_pro.py", n1, n2, n3, dir_out]);
    //
    // process.stdout.on('data', function (data) {
    //     console.log(data.toString());
    //     res.send(data.toString());
    // });

    var spawn = child_process.spawn;
    patData.forEach((pat) => {
        const mrn = pat.mrn;
        const age = pat.age;
        const sex = pat.sex;
        const testKey = pat.testKey;

        console.log(pat);

        var process = spawn('python3', ["src/PythonCode/patTest.py", mrn, age, sex, testKey, db_file_dir, upDirPath]);
        process.stdout.on('data', function (data) {
            let result_str = data.toString();
            const ind = result_str.search('Done');      // use this as a label to sort out output
            const patResult = result_str.substring(ind+5, result_str.length);

            // let tmp_arr = result_str.split(" ");
            // let result_json = {result: tmp_arr[0], proba: tmp_arr[1], comment: tmp_arr[2]};

            results_avail += 1;

            var tmp = patResult.split(" ");
            var xx = {
                "mrn": tmp[0],
                "age": tmp[1],
                "sex": tmp[2],
                "testKey": tmp[3],
                "result": tmp[4],
                "proba": tmp[5],
                "comment": tmp.slice(6, tmp.length).join(' ').trim(),
            }
            console.log('results_avail:', results_avail);
            cadResult = JSON.stringify(xx);
            result_data.push(cadResult);
            console.log(JSON.parse(cadResult));
        });
        process.stderr.on('data', (data) => {
            console.log(data.toString());
        });
    });
    res.json({message: 'Python is running'});
})

router.post('/poll', (req, res) => {
    const reqDataSize = req.body;
    const dataSize = reqDataSize.dataLength;
    let data = {
        numProcessed: results_avail,
        dataArray: result_data
    }
    res.json(data);
    // reset if all data were computed 
    if (results_avail === dataSize){
        results_avail = 0;
        result_data = [];
    }
});

router.post('/pcg', (req, res) => {
    let data = req.body;
    console.log('req data:');
    console.log(data);
    res.send('Data PCG received');
})

module.exports = router;
