const express = require('express');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const router = express.Router();
const postObj = require('./post');
const upDirPath = path.join(__dirname, '../../uploads');
// var db_file = path.join(upDirPath, 'data.db');

router.get('/', (req, res) => {
    res.send('This is base get route');
    console.log('base get route');
})

router.get('/patInfo', (req, res) => {
    console.log('Querying data.db database');
    let date = new Date();
    let m = (date.getMonth() + 1).toString();
    let ddate = 'Data' + '_' + date.getDate() + '_' + m + '_' + date.getFullYear();
    let data_path = path.join(upDirPath, ddate);
    let db_file = path.join(data_path, 'data.db');

    let db = new sqlite3.Database(db_file);
    // let sql_str = 'SELECT mrn, dob, isMale, testKey FROM PATIENT_RECORD PR JOIN TEST_INSTANCE_PATIENT_INFO TIPI ' +
    //     'ON PR.key=TIPI.patientKey';
    let sql_str = 'SELECT mrn, dob, isMale, TIPI.testKey, TEST_INSTANCE.dateAndTime ' +
        'FROM PATIENT_RECORD PR INNER JOIN ' +
        'TEST_INSTANCE_PATIENT_INFO TIPI ON PR.key=TIPI.patientKey' +
        ' INNER JOIN TEST_INSTANCE ON TEST_INSTANCE.testKey=TIPI.testKey';

    db.all(sql_str, [], (err, row) => {
        if (err) {
            console.log(err);
        }
        let patInfo = []
        row.forEach((row) => {
            var patData = {
                mrn: row.mrn,
                dob: row.dob,            // use date of test!!!
                isMale: row.isMale,
                testKey: row.testKey,
                dateTime: row.dateAndTime
            }
            let val = parseInt(patData.dateTime.split('-')[0]);
            patData.age = val - parseInt(patData.dob);

            patInfo.push(patData);
        });
        // patInfo = JSON.stringify(patInfo);
        db.close();
        res.send(patInfo);
    })
});

// router.get('/poll', (req, res) => {
//     res.status(200).send(numProcessed.toString());
// })

module.exports = router;