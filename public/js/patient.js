
//Main js code
$(function() {
    // Instantiate DataTable with options
    var table = $('#table_id').DataTable({
        paging: false,
        bInfo: false,
        bFilter: false,       //Search box
        scrollY: "70vh"
    });
    // Select rows
    $('#table_id tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );


    // This is example how to update row and change bd color
    // $('#help').on('click', () => {
    //     let resDat = {  "mrn":"001001",
    //                     "age":"28",
    //                     "sex":"Male",
    //                     "testKey":"02d2c6245f1311ea8743fdca4b84758e",
    //                     "result": "Negative",
    //                     "proba":"0.572",
    //                     "comment":"CAD is unlikely"};
    //     let tk = resDat.testKey;
    //     let resCAD = resDat.result;
    //     let proba = resDat.proba;
    //     let com = resDat.comment + ' [' + proba.toString() + ']';
    //
    //     // select row with specific testKey in 5th column
    //     var index = table.rows().eq( 0 ).filter( function (rowIdx) {
    //         return table.cell( rowIdx, 5 ).data() === tk ? true : false;
    //     } );
    //     let rowInd = index[0];
    //     table.cell({row: rowInd, column: 6}).data(resCAD);
    //     table.cell({row: rowInd, column: 7}).data(com);
    //     let row =table.rows(index);
    //     table.rows(index)
    //         .nodes()
    //         .to$()
    //         .addClass('table-success');
    //     console.log(row);
    // })

    // upload database file
    let elem = document.getElementById('customFile');
    elem.addEventListener('change', () => {
        // console.log("Event received ...");
        let files = $('#customFile').get(0).files;
        let dbfile = files[0].name;
        document.getElementById('lbl').innerHTML = dbfile;
        $('#customFile').attr("value", dbfile);
        console.log(dbfile);
        const route = '/api/post/upload'
        upload(files, route);
    });

    // read database data and fill the table
    $("#addPat").on("click", () => {
        let data = [];
        table.clear().draw();
        $.get('/api/get/patInfo', (patData) => {
            $.each(patData, function(ind, val) {
                let gen = 'Female';
                if (val.isMale === 'TRUE') {
                    gen = 'Male';
                }
                data.push([ind+1, val.mrn, val.age, gen, val.dateTime, val.testKey, " ", " "]);
            });
            table.rows.add(data).draw();
        });
    });

    // upload data files from table
    $('#up').on("click", () => {
        $('#rec_data').trigger('click');
    });
    let upButt = document.getElementById('rec_data');
    upButt.addEventListener('change', () => {
        let dataFiles = $('#rec_data').get(0).files;
        uploadData(table, dataFiles);
    })

    // Remove selected
    $('#rmPat').on("click", () => {
        table.rows('.selected').remove().draw();
    });

    // run model for classification
    $('#model').on("click", () => {
        //console.log('Running Python script');
        let mrn_data = table.column(1).data();
        let age_data = table.column(2).data()
        let sex_data = table.column(3).data()
        let testKey = table.column(5).data();
        let patData = []
        for (var i = 0; i < mrn_data.length; i++) {
            let pdata = {
                mrn: mrn_data[i],
                age: age_data[i],
                sex: sex_data[i],
                testKey: testKey[i]
            }
            patData.push(pdata);
        }
        // console.log(JSON.stringify(patData));
        // create ajax request
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/api/post/run_model', true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.responseType = "json";
        xhr.send(JSON.stringify(patData));
        console.log('Waiting for computation complete');

        xhr.onload = function () {
            if(this.status === 200) {
                console.log('Status:', xhr.status);
            }
        }
        xhr.onerror = function () {
            console.log("Error in the model")
        }
        // Check if computation completed
        polling(table, patData);
    });

    // clear table
    $('#cl').on("click",() => {
        table.clear().draw();
    });

    // // help info
    // $('#help').on("click",() => {
    //     // let table = $('#table_id').DataTable();
    //     // table.cell({row:0, column:4}).data('Negative');
    //     let dd = $('#table_id tbody').row(this).data();
    //     console.log(dd);
    // })

    $('#pcg').on('click', () => {
       let row = table.rows('.selected').data();
       if(row.length !== 1) {
           alert("Select one patient record to display")
       } else {
           // console.log(row[0]);
           let patData = {
               mrn: row[0][1],
               testKey: row[0][5]
           }
            console.log(patData);

           $.ajax({
               type: 'POST',
               url: '/api/post/pcg',
               data: JSON.stringify(patData),
               // processData: false,
               // contentType: false,
               error: function(xhr, textStatus, errThrown) {
                   alert('Error in PCG processing! ' + errThrown);
               },
               success: function(data) {
                   console.log(data);
               }
           });
       }

    });


});  // end of main

// ###################################################

function polling(table, dataSend) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/post/poll', true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.responseType = "json";
    var dataLen = {
        dataLength: dataSend.length
    }
    xhr.send(JSON.stringify(dataLen));
    xhr.onload = function () {
        if(this.status === 200) {
            // console.log(this.response);
            let dataRes = this.response;
            // console.log(dataRes);
            if (dataRes.numProcessed !== 0) {
                let dataCAD = JSON.parse(dataRes.dataArray[dataRes.numProcessed - 1]);
                let tk = dataCAD.testKey;
                let testResult = dataCAD.result;
                let proba = dataCAD.proba;
                let comm = dataCAD.comment + ' [' + proba.toString() + ']';
                let bg_color;
                switch(testResult) {
                    case 'Negative':
                        bg_color = 'table-success';
                        break;
                    case 'Positive':
                        bg_color = 'table-danger';
                        break;
                    case 'Undetermined':
                        bg_color = 'table-warning';
                        break;
                    default:
                        bg_color = '';
                }
                // select row with specific testKey in 5th column
                var index = table.rows().eq( 0 ).filter( function (rowIdx) {
                    return table.cell( rowIdx, 5 ).data() === tk ? true : false;
                } );
                let rowInd = index[0];
                table.cell({row: rowInd, column: 6}).data(testResult);
                table.cell({row: rowInd, column: 7}).data(comm);
                // let row =table.rows(index);
                table.rows(index)
                    .nodes()
                    .to$()
                    .addClass(bg_color);
            }

            if (dataRes.numProcessed !== dataSend.length) {
                setTimeout( () => {
                    polling(table, dataSend);
                }, 5000);
            } else {
                console.log('Computation completed...')
            }


            // if (dataRes.numProcessed === numTot) {
            //     // console.log(dataRes.numProcessed);
            //     console.log(dataRes.dataArray);
            //     let table = $('#table_id').DataTable();
            //     table.rows.add(dataRes.dataArray).draw();
            // } else {
            //     console.log("Patients ready:", dataRes.numProcessed);
            //     setTimeout( () => {
            //         polling(dataSend);
            //     }, 10000);
            // }
        }
    }
    xhr.onerror = function() {
        console.log('Error in polling')
    }
}

// upload data.db file
function upload(files, route) {
    if (files.length > 0) {
        let fd = new FormData();
        for (var i = 0; i < files.length; i++) {
            let file = files[i];
            fd.append('uploads[]', file, file.name);
        }
        $.ajax({
            url: route,
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false,
            error: function(xhr, textStatus, errThrown) {
                alert('Error in file upload! ' + errThrown);
            },
            success: function(data) {
                console.log(data);
            }
        });
    }
}

function uploadData(table, dataFiles) {
    var data_rows = table.rows().data();
    // console.log(data_rows);
    // console.log(dataFiles);
    let dataSend = [];
    for (var i=0; i< data_rows.length; i++) {
        // NOTE: assume that 4th colum is testKey
        const testKey = data_rows[i][5];
        let tstLabel = testKey.slice(0,6);
        for (var j=0; j< dataFiles.length; j++) {
            let fname = dataFiles[j].name;
            // assume that name contains first 6 chars of test key
            let tst = fname.split("_");
            if (tst[0] === 'recording' && tst[1] === tstLabel) {
                dataSend.push(dataFiles[j])
            }
            if (tst[0] === 'Gain' && tst[1] === tstLabel) {
                dataSend.push(dataFiles[j])
            }
        }
    }
    if ( dataSend.length < data_rows.length) {
        console.log(dataSend.length, data_rows.length);
        alert('Warning! Some recording data are missing.\n Update table and try again.');
        return;
    }
    // console.log(dataSend);
    upload(dataSend, '/api/post/upload_data');


    // console.log('Uploading data');
    // console.log(dataFiles);

}

