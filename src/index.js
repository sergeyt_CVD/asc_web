const path = require('path');
// const formidable = require('formidable');
const fs = require('fs');
// const sqlite3 = require('sqlite3').verbose();
// const child_process = require('child_process');
const express = require('express');
const postRoute = require('../api/post');
const getsRoute =require('../api/get');
const pubDirPath = path.join(__dirname, '../public');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.get('/', function(request, response) {
    response.sendFile(path.join(__dirname, '../public/login.html'));
});

app.post('/auth', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    // authenticate with db. Now pass everything
    const check = true;
    if (check) {
        console.log('Username and password accepted');
        res.sendFile(pubDirPath + '/index.html');
    }
    else {
        res.send('Please enter Username and Password');
        res.end();
    }
})

app.use(express.static(pubDirPath));
app.use('/api/post', postRoute);
app.use('/api/get', getsRoute);


// app.listen(3000, '192.168.0.8', () => {
app.listen(3000, () => {
    console.log('Server is up on port 3000');
});
