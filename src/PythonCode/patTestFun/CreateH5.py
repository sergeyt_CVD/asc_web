#!/usr/bin/python3

import cvdanalyze as cvd
import os, sys
import numpy as np
import wavio
from datetime import datetime
import h5py
import logging
import time
import argparse

VERSION = '0.1.0'


def to_coreH5(h5_path, test, tmiFileLocation):
    """
    Writes a caddet test to an h5 file

    Parameters
    ----------
    h5_path : str
        Full path of an h5 file. If no file exists it will be created

    test : cvdanalyze.Test
        Test object that will be written to the h5 file

    tmiFileLocation : str
        Directory containing the gain and recording files associated with 'test'

    """

    if not isinstance(test, cvd.Test):
        raise TypeError("test must be a cvdanalyze test object")

    if not isinstance(h5_path, str):
        raise TypeError("h5_path must be string")

    if not isinstance(h5_path, str):
        raise TypeError("tmiFileLocation must be string")

    if not os.path.isdir(tmiFileLocation):
        raise LookupError("tmiFileLocation directory not found")

    with h5py.File(h5_path, mode='a') as h5:
        h5.attrs['version'] = VERSION
        h5.attrs['last_updated'] = datetime.now().strftime("%Y-%m-%d, %H:%M:%S")
        group_name = '{}_{}_{}'.format(test.study_name, test.mrn, test.testkey)

        if group_name not in h5.keys():
            # get Gain and recording files
            files = test.files(abs_filepath=True)
            files = files[files['fileName'].str.contains('Gain|recording') == True]

            if not files.empty:

                g = h5.create_group(group_name)

                bh_counter = 0
                for idx, f in files.iterrows():
                    try:
                        tmi = wavio.read('{}/{}'.format(tmiFileLocation, os.path.basename(f.fileName)))
                    except Exception as e:
                        print('ERROR --- ', e)
                        logging.error('Exception occurred. Message {}'.format(e))
                    else:
                        assert test.rm_adc_bit_depth / 8 == tmi.sampwidth
                        assert test.rm_adc_sampling_rate == tmi.rate

                        bhs = test.breath_hold_indices(filekey=f.fileKey)
                        for _, bh in bhs.iterrows():
                            data = tmi.data[bh.startIndex:bh.stopIndex]
                            d = g.create_dataset('bh' + str(bh_counter), data=data, dtype=np.int32)
                            d.attrs['caddet:filekey'] = bh.fileKey
                            d.attrs['caddet:filename'] = os.path.basename(f.fileName)
                            d.attrs['caddet:segment'] = bh.segmentKey
                            d.attrs['caddet:startIndex'] = bh.startIndex
                            d.attrs['caddet:stopIndex'] = bh.stopIndex
                            d.attrs['caddet:classifierKey'] = bh.classifierKey
                            bh_counter += 1

                if not bh_counter:
                    print('Warning --- {} has no BHs - skipping group'.format(group_name))
                    logging.warning('{} has no BHs - skipping group'.format(group_name))
                    del h5[group_name]
                else:

                    if bh_counter < 3:
                        print('Warning < 3 Bhs for ', group_name)
                        logging.warning('{} has < 3 Bhs'.format(group_name))

                    # Log messages
                    print('Added to HDF5: GroupName = {}; BHs = {}; '
                          'TmiFileLocation = {}'.format(group_name, bh_counter, tmiFileLocation))
                    logging.info('Added to HDF5: GroupName = {}; BHs = {};'
                                 'TmiFileLocation = {}'.format(group_name, bh_counter, tmiFileLocation))

                    # add group attributes
                    g.attrs['caddet:mrn'] = test.mrn
                    g.attrs['caddet:gender'] = test.patient_gender
                    g.attrs['caddet:fs'] = test.rm_adc_sampling_rate
                    g.attrs['caddet:num_of_bhs'] = bh_counter
                    g.attrs['caddet:filenames'] = list(files.fileName.apply(lambda x: os.path.basename(x)))
                    g.attrs['caddet:dob'] = test.patient_dob
                    g.attrs['caddet:dock_id'] = test.dock_id
                    g.attrs['caddet:sw_version'] = test.software_version
                    g.attrs['caddet:fw_version'] = test.rm_firmware_version
                    g.attrs['caddet:rm_fpga_analog_gain'] = test.rm_fpga_analog_gain
                    g.attrs['caddet:rm_adc_bit_depth'] = test.rm_adc_bit_depth
                    g.attrs['caddet:date_and_time'] = str(test.date_and_time)
                    g.attrs['caddet:exam_room'] = test.exam_room
                    g.attrs['caddet:location'] = test.location
                    g.attrs['caddet:study'] = test.study_name
                    g.attrs['caddet:operator-firstName'] = test.operator_firstname
                    g.attrs['caddet:operator-lastName'] = test.operator_lastname
                    g.attrs['caddet:operator-userName'] = test.operator_username
                    g.attrs['caddet:testkey'] = test.testkey

            else:
                print('Warning --- {} has no BHs - skipping group'.format(group_name))
                logging.warning('{} has no BHs - skipping group'.format(group_name))
        else:
            print('GroupName {} already exists in HDF5 file'.format(group_name))
            logging.info('GroupName {} already exists in HDF5 file'.format(group_name))


def convert_caddet_to_h5(db_path, output_folder=None, testkey=None, mrn=None, study=None):
    with cvd.DataBase(os.path.join(db_path)) as db:
        assert db.version == 4, 'DB version must be 4'

        if testkey:
            if not isinstance(testkey, list):
                tests = [testkey]
            else:
                tests = testkey
        elif mrn:
            df_tests = db.testkeys(study=study, have_acoustics=False, mrn=mrn)
            tests = df_tests.testKey.to_list()
        else:
            df_tests = db.testkeys(study=study, have_acoustics=False)
            tests = df_tests.testKey.to_list()

        for tk in tests:
            test = cvd.Test(db=db, testkey=tk)
            h5_filename = os.path.join(str(output_folder or ''), '{}_{}.h5'.format(test.mrn, test.testkey))
            to_coreH5(h5_filename, test, os.path.dirname(db_path))


if __name__ == '__main__':
    argv = sys.argv[1:]
    mrn = argv[0]
    testKey = argv[1]
    dir_rec = argv[2]  # full path to data.db and file itself
    data_db = dir_rec + '/data.db'
    dir_out = argv[3]

    # generate h5 for selected mrn testKey combination
    convert_caddet_to_h5(data_db, output_folder=dir_out, mrn=mrn, testkey=testKey)

    # parser = argparse.ArgumentParser(description='This utility converts a cad-det patient file to coreH5 format')
    #
    # parser.add_argument('--output_folder', type=str, dest='output_folder', default=None, required=False,
    #                     help='Location to output the generated .h5 and log files')
    # parser.add_argument('--mrn', type=str, dest='mrn', default=None, required=False,
    #                     help='Generate file for specified MRN only, the output *.h5 will be named after the MRN')
    # parser.add_argument('--study', type=str, dest='study', default=None, required=False,
    #                     help='Generate file for specified STUDY only')
    # parser.add_argument('--patient_file', type=str, dest='patient_file', required=True,
    #                     help='Path to the Caddet patient directory that contains the database and recordings')
    #
    # args = parser.parse_args()
    # mrn = args.mrn
    # study = args.study
    # output_folder = args.output_folder
    # patient_file = args.patient_file
    #
    # if output_folder:
    #     if not os.path.isdir(output_folder):
    #         os.mkdir(output_folder)
    #
    # # Logging
    # logging.basicConfig(filename=os.path.join(str(output_folder or ''),
    #                     'runLog_{}.log'.format(time.strftime("%Y%m%dT%H%M%S"))),
    #                     filemode='a', format='%(levelname)8s %(asctime)s %(filename)s:%(lineno)4d %(message)s',
    #                     level=logging.DEBUG)
    #
    # db_path = os.path.join(patient_file, 'data.db')
    # print(db_path)
    # if not os.path.isfile(db_path):
    #     FileNotFoundError('{} does not exist'.format(db_path))

