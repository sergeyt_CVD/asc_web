
import pandas as pd
import numpy as np
import scipy.signal as signal

import peakutils
import warnings
from numpy.lib import stride_tricks
from numpy import polyfit, poly1d
from scipy.signal import savgol_filter
from scipy.stats import median_absolute_deviation

# from .constants import *

def segment_ecg(sig, fs, downsampling_factor=None,
                lowpass_filter_type='butter', cutoff_freq=40, filter_order=5, rp_rs=None,   # LP params
                sg_order=2, fs_fraction=0.3,                                                # HP, SG params
                peak_width=0.021, sh_power=4):                                              # shannon envelope params
    """
    Segments R-R peak intervals from a given ECG signal.  The intervals are provided as a list of tuples
    separated into accepted and rejected beats.  Stats for the accepted beats are also provided

    Parameters
    ----------
    sig : ndarray
        1-d ndarray of ECG signal to be segmented.
    fs : int
        Sampling Frequency
    downsampling_factor: int or None, optional
        Downsampling factor.
    lowpass_filter_type: str, optional
        Lowpass filter type; one of ['butter', 'cheby1', 'cheby2']. Default is 'butter'
    cutoff_freq : int, optional
        A scalar or length-2 sequence giving the critical frequencies.  Default is 40
    filter_order :  int, optional
        The order of the filter.  Default is 5
    rp_rs : float, optional
        Used with cheby1 filter; the maximum ripple allowed below unity gain in the passband.
        Used with cheby2 filter; the minimum attenuation required in the stop band.
        Specified in decibels, as a positive number.
        Not used with butterworth lowpass filter
    sg_order: int, optional
        The order of the polynomial used to fit the samples. Default is 2
    fs_fraction : float, optional
        Fractional sampling frequency window. Window size as a function of sampling frequency.  Default is 0.3
    peak_width : float, optional
         Used when evaluating the window size for the shannon_envelop. Default is 0.021
    sh_power : int, optional
        Power operator used when evaluating the Shannon Envelope.  Should be an even number.  Default is 4

    Returns
    -------
    rr_accepted: list of tuples
        Start and end indices of accepted ECG R-R interval
    rr_declined : list of tuples
        Start and end indices of rejected ECG R-R interval
    rr_stats : dict
        Stats from the accepted R-R intervals (rr_accepted).  This is the output of ecg_rr_stats() with the addition
        of the quality metric
    """

    def quality(sig, sig_filt, win_size, n_samples):
        """
        Computes quality of filtered signal vs true signal.
        The maximum value of n_samples windows defined by win_size are found from sig sig_filt.
        A ratio of the medians from the maximums are used to define quality, q

        """
        samples_sig = np.zeros((win_size, n_samples))
        samples_sig_filt = np.zeros((win_size, n_samples))
        for x in range(n_samples):
            r = np.random.randint(win_size, len(sig)-win_size)
            samples_sig[:, x] = sig[r:r+win_size]
            samples_sig_filt[:, x] = sig_filt[r:r+win_size]
        sig_filt_max = np.max(samples_sig_filt, axis=0)
        sig_max = np.max(samples_sig, axis=0)
        q = np.median(sig_filt_max/sig_max)
        return q

    def update_peaks_using_original_signal(sig, peaks, win_size):
        """
         Finds the peak from the original signals to ensure the true peak is found

        """
        num_peaks = peaks.shape[0]
        peaks_update = peaks.copy()
        win_size = win_size // 2
        sig_len = len(sig)
        for p in range(num_peaks):
            win_start = peaks[p] - win_size if peaks[p] - win_size >= 0 else 0
            win_end = peaks[p] + win_size if peaks[p] + win_size <= sig_len else sig_len
            temp_sig = sig[win_start:win_end]
            peaks_update[p] = win_start + temp_sig.argmax()

        return peaks_update

    def sift_peaks(peak_beats, threshold_multiplier=0.15):
        """
        Sift beats that do not fall with a range from median determined by threshold_multiplier
        Peaks are sifted into accepted and rejected lists

        Returns list of tuple pairs labeled as accepted and rejected.
        As well as stats (mean, median, var) for accepted beats

        """
        if threshold_multiplier < 0:
            raise ValueError('threshold_multiplier must be > 0')

        rr_length = [(x[1] - x[0]) for x in peak_beats]
        rr_med = np.median(rr_length)
        threshold_range = threshold_multiplier * rr_med

        beats_accepted, beats_rejected = [], []
        for idx, rr in enumerate(rr_length):
            if abs(rr - rr_med) < threshold_range:
                beats_accepted.append(peak_beats[idx])
            else:
                beats_rejected.append(peak_beats[idx])

        return beats_accepted, beats_rejected

    if downsampling_factor is not None:
        sig = decimate(sig, downsampling_factor)
        fs = fs // downsampling_factor
    else:
        sig = np.copy(sig)

    sig_orig = sig.copy()      # save original signal before filtering

    # filter signal
    # 1) LP
    sig = filtfilt_signal(sig, fs, cutoff_freq, filter_type=lowpass_filter_type,
                          filter_order=filter_order, btype='lowpass', rp_rs=rp_rs)
    # 2) SG filter (HP)
    sg_smooth = sg_filter(sig, sg_order, fs, fs_fraction)
    sig = sig - sg_smooth

    sig_filt = sig.copy()       # save filtered signal

    # compute quality
    quality = quality(sig_orig, sig_filt, win_size=int(fs), n_samples=10)

    # rectify signal
    sig = rectify(sig, how='half')

    # Normalize peaks
    sig = sig/np.max(sig)
    sig = sig * 0.8

    # Shannon Envelope
    window_size = int(np.ceil(peak_width * fs))
    sig_en = shannon_envelope(sig, window_size, step=1, sh_power=sh_power)

    # Find peaks
    sig_en = sig_en/np.max(sig_en)
    peaks_idx = _find_ecg_peaks(sig_en, fs, min_dist_thres=int(fs/3), window_size=1.6, hop_length=0.4) #TODO constants

    # fs//40 - window size should fit within QRS complex
    peaks_idx = update_peaks_using_original_signal(sig_filt, peaks_idx, fs//40)

    if downsampling_factor:
        peaks_idx = peaks_idx * downsampling_factor
        fs = fs * downsampling_factor

    # Create pairs of tuples for start and end of each beat using R-R peaks
    peak_beats = [(peaks_idx[i], peaks_idx[i + 1]) for i in range(len(peaks_idx) - 1)]

    # Sift beats into accepted and rejected list.
    # Note: Beats in rejected list may be not be due to noise but from a  physiological conditions and not
    rr_accepted, rr_declined = sift_peaks(peak_beats, threshold_multiplier=0.15)

    rr_stats = ecg_rr_stats(rr_accepted, fs)
    rr_stats['quality'] = quality       # add quality metric to the stats

    return rr_accepted, rr_declined, rr_stats


def filtfilt_signal(sig, fs, cutoff_freq, filter_type='butter', filter_order=8, rp_rs=None,
                    btype='lowpass', plot_filter=False):
    """
    TODO: Get function definition from Robin

    Parameters
    ----------
    sig : ndarray
        The array of data to be filtered.
    fs : float
        The sampling frequency of the digital system.
    cutoff_freq : int
        A scalar or length-2 sequence giving the critical frequencies.
        For a Butterworth filter, this is the point at which the gain
        drops to 1/sqrt(2) that of the passband (the "-3 dB point").
    filter_type : str, optional
        Identifies the filter to be applied
    filter_order : int, optional
        The order of the filter.
    rp_rs: float, optional
        Value is required for the cheby1 filter, processed as rp - the maximum ripple allowed below
        unity gain in the passband.
        Value is required for the cheby2 filter, precess as rs - the minimum attenuation required in the stop band.
    btype : {'lowpass', 'highpass', 'bandpass', 'bandstop'}, optional
        The type of filter.  Default is 'lowpass'.
    plot_filter : bool, optional
        Used to visualize the filter attributes, second-order sections representation of the IIR filter.

    Returns
    -------
    out : ndarray
        The filtered output with the same shape as `sig`.

    Raises
    ------
    ValueError
        This exception is raised if an error is detected when validating the function arguments
    """
    if filter_type not in ['butter', 'cheby1', 'cheby2']:
        raise ValueError('filter type not supported')

    if btype not in ['lowpass', 'highpass', 'bandpass', 'bandstop']:
        raise ValueError('btype not supported')

    if filter_type in ['cheby1', 'cheby2'] and rp_rs is None:
        raise ValueError('rp_rs not specified for Chebyshev filter')

    if filter_order > 25:
        warnings.warn('filter order is high and may lead to instability')

    if filter_type == 'butter':
        sos = signal.butter(filter_order, cutoff_freq, fs=fs, btype=btype, output='sos')
    elif filter_type == 'cheby1':
        sos = signal.cheby1(filter_order, rp_rs, cutoff_freq, fs=fs, btype=btype, output='sos')
    else:
        sos = signal.cheby2(filter_order, rp_rs, cutoff_freq, fs=fs, btype=btype, output='sos')

    if plot_filter:
        import matplotlib.pyplot as plt

        w, h = signal.sosfreqz(sos, worN=2048)
        freq = w * fs / (2 * np.pi)
        plt.semilogx(freq, 20 * np.log10(abs(h)))
        plt.grid(which='both', axis='both')
        plt.axvline(cutoff_freq, color='cyan')  # cutoff frequency
        plt.text(cutoff_freq * 0.9, -35, 'Cutoff Frequency', rotation=90)
        # plt.axvline(60, color='cyan')  # powerline frequency
        plt.title('Filter Frequency Response')
        plt.ylim([-60, 5])
        plt.show()

    return signal.sosfiltfilt(sos, sig, padtype='odd', padlen=int(fs/20))


def sg_filter(sig, polyorder, fs, ffw, deriv=0, delta=1.0, axis=-1, mode='interp', cval=0.0):
    """ Wrapper to Scipy's Savitzky-Golay filter.

    Parameters
    ----------
    sig : ndarray
          The array of data to be filtered.
    polyorder : int
          The order of the polynomial used to fit the samples. polyorder must be less than window_length.
    fs : int
          Sampling frequency
    ffw : float
        Fractional sampling frequency window. Window size as a function of sampling frequency
          Eg. If fs = 500 and expected window size needed is 250, then ffw = 0.5
    deriv : int, optional
          See Scipy savgol_filter documentation
    delta : float, optional
          See Scipy savgol_filter documentation
    axis : int, optional
          The axis of the array x along which the filter is to be applied. Default is -1.
    mode: str, optional
          See Scipy savgol_filter documentation
    cval : scalar, optional
          See Scipy savgol_filter documentation

    Returns
    -------
    out : ndarray, same shape as sig
          The filtered data
    """

    sg_window = int(fs * ffw)
    if sg_window % 2 == 0: sg_window += 1  # make odd

    return signal.savgol_filter(sig, sg_window, polyorder, deriv=deriv, delta=delta, axis=axis, mode=mode, cval=cval)


def ecg_rr_stats(rr_list, fs):
    """
    Produce stats for ECG R-R peaks

    Parameters
    ----------
    rr_list : list of tuples
        Start and end indices of ECG R-R intervals
    fs : int
        Sampling Frequency

    Returns
    -------
    stats : dict
        Returns {'mean', 'median', 'var', 'std', 'mad', 'bpm'}

    """

    if not isinstance(fs, int):
        raise ValueError('fs must be an int')
    if fs <= 0:
        raise ValueError('fs must be > 0')

    stats = {'mean': np.nan, 'median': np.nan, 'var': np.nan, 'std': np.nan, 'mad': np.nan, 'bpm': np.nan}

    if rr_list:
        beats_length = np.diff(np.array(rr_list))
        rr_median = np.median(beats_length)
        bpm = int(60 / (rr_median / fs)) if not np.isnan(rr_median) else np.nan

        # mean, median and variance in samples
        stats['mean'] = np.mean(beats_length)
        stats['median'] = rr_median
        stats['var'] = np.var(beats_length)
        stats['std'] = np.std(beats_length)
        stats['mad'] = median_absolute_deviation(beats_length)
        stats['bpm'] = bpm

    return stats

def rectify(sig, how='full'):
    """

    Parameters
    ----------
    sig : array like
          Signal to be rectified
    how : str
          Type of rectification.
          Can be 'full' or 'half'

    Returns
    -------
    out : ndarray
          Rectified signal

    Raises
    ------
    ValueError
        This exception is raised if an error is detected when validating the value/data type of the method arguments
    """

    if not isinstance(how, str):
        raise ValueError('how argument must be str')

    how = how.lower()
    if how not in ['full', 'half']:
        raise ValueError('incorrect type supplied to how argument')

    sig = np.atleast_1d(sig).copy()

    if how == 'full':
        return np.abs(sig)
    else:
        neg_ix = np.where(sig < 0)
        sig[neg_ix] = 0
        return sig


def shannon_envelope(sig, window_size, step, sh_power=4):
    """
    Function to generate the Shannon Envelope of a signal

    Parameters
    ----------
    sig : ndarray
        Signal
    window_size : int
    step : int
        Step size. If less than window size there will be overlap
    sh_power : int
        Power operator. Should be an even number. Default is 4

    Returns
    -------
    out : ndarray
        Envelope data of sig
    """

    new_sig = np.atleast_1d(sig).copy()

    new_sig = np.concatenate((np.zeros(window_size // 2), new_sig, np.zeros(window_size // 2)))
    strided_sig = stride_array(new_sig, window_size, step)

    shan = np.zeros(strided_sig.shape[0])
    pw = np.abs(strided_sig) ** sh_power
    shan = np.mean(-np.log10(pw ** pw), axis=1)

    return shan

def _find_ecg_peaks(sig_envelope, fs, min_dist_thres=1, window_size=1.6, hop_length=None,
                    min_threshold_multiplier=0.6, max_threshold_multiplier=2, minimum_sig_length=5):
    """

    Parameters
    ----------
    sig_envelope : ndarray
        1-d ecg signal data
    fs : int
        Sampling Frequency
    min_dist_thres : int
        Required minimal horizontal distance (>= 1) in samples between
        neighbouring peaks.
    window_size : float
        Size of window normalized to fs to determine the stride window
    hop_length : float or None
        Hop length normalized to fs.  Overlapping windows will have a hop length < window_size
    min_threshold_multiplier : float
        Minimum threshold boundary to detect peaks.  Set as level normalized to the minimum peak detected from windowed
        peaks
    max_threshold_multiplier : float
        Maximum threshold boundary to detect peaks.  Set as level normalized to median peak detected from windowed peaks
    minimum_sig_length

    Returns
    -------
    out : ndarray
        1-d array of indices where the ECG peaks are identified

    Raises
    ------
    ValueError
        This exception is raised if the length of sig_envelop is too short
    """

    if len(sig_envelope) < minimum_sig_length*fs:
        raise ValueError('length of sig_envelope is too short')

    if hop_length is None:
        strided_hop_length = int(fs*0.4)
    else:
        strided_hop_length = int(fs * hop_length)

    strided_sig = stride_array(sig_envelope, int(fs * window_size), strided_hop_length)
    strided_peaks = np.max(strided_sig, axis=1)

    p5, median_peak = np.percentile(strided_peaks, [5, 50])
    min_peak = np.min(strided_peaks[strided_peaks > p5])

    min_threshold = min_peak * min_threshold_multiplier
    max_threshold = median_peak * max_threshold_multiplier

    p, _ = signal.find_peaks(sig_envelope, height=min_threshold)

    # find optimal minimum distance
    min_dist = np.percentile(np.diff(p), [0, 10, 15, 20, 25, 30, 35, 40])
    min_dist = min_dist * 0.95  # ensure that the smallest distance will be captured
    min_dist = min_dist[min_dist >= min_dist_thres]     # must be greater that min threshold
    if min_dist.size == 0:
        min_dist = min_dist_thres
    else:
        min_dist = min_dist[0]

    p, _ = signal.find_peaks(sig_envelope, height=(min_threshold, max_threshold), distance=min_dist)

    return p


def stride_array(arr, frame_length, hop_length):
    """
    Return view of array with specified frame length and hop length

    Parameters
    ----------
    arr : ndarray
          Array to create a new.
    frame_length : int
          num of columns desired
    hop_length : int
          starting element of each row starting from the origin

    Returns
    -------
    view : ndarray

    Notes
    -------
    If arr is array([ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15])

    A) frame_length > hop_length, there is overlap of arr

    frame_length = 4, hop_length = 2 - the view returned will be:

    array([[ 0,  1,  2,  3],
           [ 2,  3,  4,  5],
           [ 4,  5,  6,  7],
           [ 6,  7,  8,  9],
           [ 8,  9, 10, 11],
           [10, 11, 12, 13],
           [12, 13, 14, 15]])

    B) frame_length == hop_length, there is no overlap

    frame_length = 4, hop_length = 4 - the view returned will be:

    array([[ 0,  1,  2,  3],
           [ 4,  5,  6,  7],
           [ 8,  9, 10, 11],
           [12, 13, 14, 15]])

    C) frame_length < hop_length,

    frame_length = 2, hop_length = 4 - the view returned will be:

    array([[ 0,  1],
           [ 4,  5],
           [ 8,  9],
           [12, 13]])


    As seen above, the view may not return every element

    Raises
    ------
    TypeError
        This exception is raised if dimension arguments are not integer values
    """

    if not isinstance(frame_length, int):
        raise TypeError('frame_length must be an int')

    if not isinstance(hop_length, int):
        raise TypeError('hop_length must be an int')

    num_frames = int(1 + (len(arr) - frame_length) / hop_length)
    row_stride = arr.itemsize * hop_length
    col_stride = arr.itemsize
    view = stride_tricks.as_strided(arr, shape=(num_frames, frame_length),
                                    strides=(row_stride, col_stride), writeable=False)

    return view


def decimate(sig, downsampling_factor, axis=-1):
    """
    Decimates ndarray using an order 8 Chebyshev type I anti-aliasing filter.  See scipy's decimate function

    Parameters
    ----------
    sig : ndarray
        Signal to be decimated
    downsampling_factor: int
        Downsampling Factor
    axis : int, optional
        The axis along which to decimate.

    Returns
    -------
    out : ndarray
        Downsampled sig

    Raises
    ------
    TypeError
        This exception is raised if the decimation argument is not an integer value
    """
    if downsampling_factor is not None:
        if not isinstance(downsampling_factor, int):
            raise TypeError('decimation factor must be int')

        if downsampling_factor > 10:
            warnings.warn('decimation factor ({}) is high for iir filter and may lead to instability'
                          .format(downsampling_factor))

        return signal.decimate(sig, downsampling_factor, axis=axis, zero_phase=True)
    
