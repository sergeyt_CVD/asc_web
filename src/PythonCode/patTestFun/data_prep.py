import os
import numpy as np
import pandas as pd
from CoreH5Reader import CoreH5Reader
from beat_seg import beat_seg
from plot_stack import plot_stack_s1s2_v2
from quality_test import quality_test_S1S2
from freq_domain import freq_domain
import h5py

N_MAX = 5120
S4_SIZE = 600               # number of samples for S4 region, at fs = 4000
SHOW_DATA = False

def data_prep(mrn, datah5file, testKey, age, sex, studies, channels):
    """ Preprocess data record and return in proper format """
    pat_info = [mrn, age, sex, testKey]
    bhs_dict, fs, gr_name, bh_status = read_bhs(mrn, datah5file, studies)
    stacks, data_info = beat_seg(mrn, bhs_dict, fs, bh_status, channels, sg_tw=0)
    s1_start_list = data_info['S1_START']
    s1_end_list = data_info['S1_END']
    s2_start_list = data_info['S2_START']
    s2_end_list = data_info['S2_END']

    # # Display stacks for all channels
    if SHOW_DATA:
        for ch in channels:
            i = int(ch) - 1
            st = stacks[i]
            tit = 'Channel: ' + str(ch)
            plot_stack_s1s2_v2(st, fs, s1_start_list[i], s1_end_list[i], s2_start_list[i], s2_end_list[i], title=tit)

    # Apply quality test and remove bad beats
    i = 0
    for st in stacks:
        stack_ch = None
        bad_beats = None
        s1_1, s1_2, s2_1, s2_2 = None, None, None, None
        try:
            stack_ch, bad_beats, s1_1, s1_2, s2_1, s2_2 = \
                quality_test_S1S2(st, s1_start_list[i], s1_end_list[i], s2_start_list[i], s2_end_list[i])
        except ValueError:
            print('Data appear to be poor quality ...')
            exit(0)

        # Remove zeros from tail
        nb = stack_ch.shape[0]
        ha_len = np.zeros(nb, dtype='int')
        for j in range(nb):
            x = stack_ch[j]
            beat_len = len(np.trim_zeros(x, 'b'))
            ha_len[j] = beat_len

        max_len = np.max(ha_len)
        stack_ch = stack_ch[:, 0:max_len]

        # # ## Show result of quality test
        # print('Bad beats:', bad_beats)
        # title = 'CHANNEL: ' + str(i + 1)
        # plot_stack_s1s2_v2(st, fs, s1_start_list[i], s1_end_list[i], s2_start_list[i], s2_end_list[i], title=title)
        # plot_stack_s1s2_v2(stack_ch, fs, s1_1, s1_2, s2_1, s2_2, title=title + ' Quality')

        stacks[i] = stack_ch
        s1_start_list[i] = s1_1
        s1_end_list[i] = s1_2
        s2_start_list[i] = s2_1
        s2_end_list[i] = s2_2
        i += 1

    df = build_beat_df(stacks, fs, s1_start_list, s1_end_list, s2_start_list, s2_end_list, pat_info, channels)
    return df

def read_bhs(mrn, h5_file, studies):
    """ read all pcg breath holds for all 6 channels """
    # read bh data from h5 file
    check = os.path.exists(h5_file)
    if check is False:
        print('Error! File *.h5 does not exist ...')
        exit(1)

    gr_names = []
    bh_status = []
    with h5py.File(h5_file, 'r') as fh5:
        groups_avail = list(fh5.keys())
        # serach for available data for given mrn and studies list
        for study in studies:
            nm = study + '_' + str(mrn)
            for gr in groups_avail:
                if nm in gr:
                    gr_names.append(gr)

    if len(gr_names) == 0:
        print('No groups for this patient were found')
        print('Avail groups:', groups_avail)
        print('Avail studies:', studies)
        exit(1)

    if len(gr_names) != 1:
        print('There multiple recordings for patient:', mrn)

        print(gr_names)

        for i in range(len(gr_names)):
            print(str(i + 1) + ')', gr_names[i])
        ii = input('Select data to test: ')
        ii = int(ii) - 1
        gr_name = gr_names[ii]
    else:
        gr_name = gr_names[0]
        # print('Patient:', mrn)
        # print('Process test_record:', gr_name)
    with CoreH5Reader(h5_file) as h5:
        testKey = h5.testkey(group_name=gr_name)
        bhs = h5.bhs(testKey, normalize=True)
        fs = h5.get_attributes([gr_name], 'caddet:fs')
        for bh in bhs:
            bh_atts = h5.get_attributes([gr_name, bh])
            bh_status.append(bh_atts['caddet:classifierKey'])
    return bhs, fs, gr_name, bh_status

def build_beat_df(stacks, fs, s1_1_list, s1_2_list, s2_1_list, s2_2_list, pat_demo, channels2use):
    """ Build single dataframe for all channels """
    mrn = pat_demo[0]
    age = pat_demo[1]
    sex = pat_demo[2]
    testKey = pat_demo[3]

    colnames = ['V' + str(i) for i in range(N_MAX)] + ['S1_START'] + ['S1_END'] + ['S2_START'] + \
               ['S2_END'] + ['S4_START'] + ['S4_END'] + \
               ['CH'] + ['MRN'] + ['TestKey'] + ['Age'] + ['Sex']
    df = pd.DataFrame(columns=colnames)

    # df_features = pd.DataFrame([])

    nback = 81
    alpha = 0.3
    x_w = np.arange(nback)
    x_w = x_w - (nback - 1) // 2
    wb = 1 / (1 + np.exp(alpha * x_w))

    n_ch = len(stacks)
    # loop over channels
    # print('Feature Extraction ...')
    for j in range(n_ch):
        # print('Channel:', channels2use[j])
        s1_start = s1_1_list[j]
        s1_end = s1_2_list[j]
        s2_start = s2_1_list[j]
        s2_end = s2_2_list[j]
        stack = stacks[j]

        nbeats = stack.shape[0]
        for ii in range(nbeats):
            sx = np.zeros(N_MAX, dtype=np.float32)
            s = stack[ii]
            s = np.trim_zeros(s, 'b')
            s[-nback:] = s[-nback:] * wb
            # s = sig_norm_rms(s)
            ns = len(s)

            s4_start = ns - S4_SIZE
            s4_end = ns

            # print('Sig power:', np.mean(s ** 2))
            # zero-padded signal
            if ns <= N_MAX:
                sx[0:ns] = s
            else:
                sx = s[0:N_MAX]
                s4_start = N_MAX - S4_SIZE
                s4_end = N_MAX

            df.loc[len(df)] = list(sx) + [s1_start[ii]] + [s1_end[ii]] + [s2_start[ii]] + [s2_end[ii]] + \
                              [s4_start] + [s4_end] + [channels2use[j]] + [mrn] + [testKey] + [age] + [sex]

    df['S1_START'] = df['S1_START'].astype('int')
    df['S1_END'] = df['S1_END'].astype('int')
    df['S2_START'] = df['S2_START'].astype('int')
    df['S2_END'] = df['S2_END'].astype('int')
    df['S4_START'] = df['S4_START'].astype('int')
    df['S4_END'] = df['S4_END'].astype('int')

    # Data check
    # print(list(set(df.CH.values)))
    # print(df.shape)

    df_features = freq_domain(df, channels2use, fs)
    return df_features
