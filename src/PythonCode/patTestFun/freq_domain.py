import sys, os
import numpy as np
import pandas as pd
import pywt
from obspy.signal.tf_misfit import *
# import matplotlib
# matplotlib.use('Qt5Agg')
# import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1 import make_axes_locatable
# from matplotlib.widgets import Cursor

# WPT_LEVEL = 6
# WAVELET = 'db6'
# N_BANDS = 8

SHOW_CWT_BEATS = False
SHOW_CWT_AVER = False

def freq_domain(df, CHANNELS, fs):
    """
    Compute frequency domain features using wpt decomposition
    :param df:
    :param CHANNELS:
    :param fs:
    :return:
    """
    mrn = df['MRN'].values[0]
    age = df['Age'].values[0]
    sex = df['Sex'].values[0]
    testkey = df['TestKey'].values[0]

    # CWT parameters
    fmin = 5.0
    fmax = 300.0
    w0 = 6
    n_bands = 16     # number of freq bins

    cnames = ['SP' + str(i) for i in range(5 * n_bands)]
    colnames = ['MRN', 'Age', 'Sex', 'TestKey', 'CH'] + cnames
    df_features = pd.DataFrame(columns=colnames)

    for ch in CHANNELS:
        df_ch = df.loc[df['CH'] == int(ch)]

        # make sure there are beats in the channel
        if len(df_ch) == 0:
            continue

        # mean spectrum for all beats in channel
        sp_s1_mean = np.zeros(n_bands, dtype=np.float32)
        sp_sys_mean = np.zeros(n_bands, dtype=np.float32)
        sp_s2_mean = np.zeros(n_bands, dtype=np.float32)
        sp_dia_mean = np.zeros(n_bands, dtype=np.float32)
        sp_s4_mean = np.zeros(n_bands, dtype=np.float32)

        mean_vals = [mrn, age, sex, testkey, ch]
        # scan over beat for single channel
        cwt_mean = np.zeros((n_bands, 5120))
        sx = np.zeros(5120)
        nbeats = 0      # beat counter
        for ix, row in df_ch.iterrows():
            sig = row['V0':'V5119'].values
            s1_start = row['S1_START']
            s1_end = row['S1_END'] + 100
            s2_start = row['S2_START']
            # s2_end = row['S2_END'] + 200
            s2_end = row['S2_END'] + 100
            s4_start = row['S4_START']
            s4_end = row['S4_END']

            if s2_end >= s4_start:
                continue

            # Compute CWT
            wt = cwt(sig, 1 / fs, w0, fmin, fmax, nf=n_bands)
            wt = np.abs(wt)

            cwt_mean = cwt_mean + wt
            sx = sx + sig

            # print('CH:', ch, 'ix:', ix)
            # print(s1_start, s1_end)
            # print(s1_end, s2_start)
            # print(s2_start, s2_end)
            # print(s2_end, s4_start)
            # print(s4_start, s4_end)
            # print('|-------------------------|')

            # Compute mean spectral amplitude for sub-regions
            sp_s1 = np.mean(wt[:, s1_start:s1_end], axis=1)
            sp_s1_mean = sp_s1_mean + sp_s1
            sp_sys = np.mean(wt[:, s1_end:s2_start], axis=1)
            sp_sys_mean = sp_sys_mean + sp_sys
            sp_s2 = np.mean(wt[:, s2_start:s2_end], axis=1)
            sp_s2_mean = sp_s2_mean + sp_s2
            if (s4_start - s2_end) < 800:
                sp_dia = np.mean(wt[:, s2_end:s4_start], axis=1)
            else:
                sp_dia = np.mean(wt[:, s2_end:(s2_end + 800)], axis=1)
            sp_dia_mean = sp_dia_mean + sp_dia
            sp_s4 = np.mean(wt[:, s4_start:s4_end], axis=1)
            sp_s4_mean = sp_s4_mean + sp_s4

            nbeats += 1

            # # # ###### plot pics
            if SHOW_CWT_BEATS:
                s1_1 = (s1_start) / fs
                s1_2 = (s1_end) / fs
                s2_1 = (s2_start) / fs
                s2_2 = (s2_end) / fs
                s4_1 = (s4_start) / fs
                s4_2 = (s4_end) / fs
                f_top = 300.0
                s1s2s4 = [s1_1, s1_2, s2_1, s2_2, s4_1, s4_2, f_top]

                t = np.arange(len(sig))/fs
                plt.plot(t, sig)
                plt.plot([s1_1, s1_1], [-2, 2], color='r')
                plt.plot([s1_2, s1_2], [-2, 2], color='r')
                plt.plot([s2_1, s2_1], [-2, 2], color='r')
                plt.plot([s2_2, s2_2], [-2, 2], color='r')
                plt.plot([s4_1, s4_1], [-2, 2], color='r')
                plt.plot([s4_2, s4_2], [-2, 2], color='r')
                plt.show()

                plot_cwt(wt, sig, fmin, fmax, fs, s1s2s4=s1s2s4)

                plt.plot(sp_s1)
                plt.plot(sp_sys)
                plt.plot(sp_s2)
                plt.plot(sp_dia)
                plt.plot(sp_s4)
                plt.show()

                im = np.zeros((5, n_bands))
                im[0] = sp_s1
                im[1] = sp_sys
                im[2] = sp_s2
                im[3] = sp_dia
                im[4] = sp_s4
                plt.imshow(im, origin='lower', cmap='jet')
                figManager = plt.get_current_fig_manager()
                figManager.window.showMaximized()
                plt.show()

        if nbeats == 0:
            continue
        sp_s1_mean = sp_s1_mean/nbeats
        sp_sys_mean = sp_sys_mean/nbeats
        sp_s2_mean = sp_s2_mean/nbeats
        sp_dia_mean = sp_dia_mean/nbeats
        sp_s4_mean = sp_s4_mean/nbeats

        if SHOW_CWT_AVER:
            cwt_mean = cwt_mean/nbeats
            sx = sx/nbeats
            s1_1 = (s1_start) / fs
            s1_2 = (s1_end) / fs
            s2_1 = (s2_start) / fs
            s2_2 = (s2_end) / fs
            s4_1 = (s4_start) / fs
            s4_2 = (s4_end) / fs
            f_top = 300.0
            s1s2s4 = [s1_1, s1_2, s2_1, s2_2, s4_1, s4_2, f_top]
            plot_cwt(cwt_mean, sx, fmin, fmax, fs, s1s2s4=s1s2s4)

            plt.plot(sp_s1_mean)
            plt.plot(sp_sys_mean)
            plt.plot(sp_s2_mean)
            plt.plot(sp_dia_mean)
            plt.plot(sp_s4_mean)
            plt.show()

            im = np.zeros((5, n_bands))
            im[0] = sp_s1_mean
            im[1] = sp_sys_mean
            im[2] = sp_s2_mean
            im[3] = sp_dia_mean
            im[4] = sp_s4_mean
            plt.imshow(im, origin='lower', cmap='jet')
            plt.title('MRN: ' + mrn + ' Channel: ' + str(ch))
            figManager = plt.get_current_fig_manager()
            figManager.window.showMaximized()
            plt.show()

        mean_vals = mean_vals + list(sp_s1_mean) + list(sp_sys_mean) + list(sp_s2_mean) + \
                    list(sp_dia_mean) + list(sp_s4_mean)
        df_features.loc[len(df_features)] = mean_vals

    # print(df_features)
    return df_features


def plot_cwt(sclgrm, sig, fmin, fmax, fs, title='', s1s2s4=None, file_png=None):
    """
    Plot cwt image with original time domain signal
    :param cwt:
    :param FS:
    :param title:
    :return:
    """
    fig, ax = plt.subplots(figsize=(16, 10))
    mngr = plt.get_current_fig_manager()
    mngr.window.setGeometry(100, 200, 900, 800)
    Nx = sclgrm.shape[1]
    Ny = sclgrm.shape[0]
    t = np.arange(Nx) / fs
    f = np.logspace(np.log10(fmin), np.log10(fmax), Ny)
    # df = (fmax - fmin)/Ny
    # f = fmin + np.arange(Ny)*df

    # Normalize scalogram using standard norm
    # sclgrm = norm_im(sclgrm)

    sclgrm_max = np.max(np.max(sclgrm, axis=1))
    sclgrm_min = np.min(np.min(sclgrm, axis=1))
    # print('Sclgrm min:', sclgrm_min)
    # print('Sclgrm max:', sclgrm_max)
    #
    # print('Scalogram normalized using min/max values.')
    sclgrm = (sclgrm - sclgrm_min) / (sclgrm_max - sclgrm_min)

    # scale_fact = 34
    # print('CWT scaling factor:', scale_fact)
    # sclgrm = sclgrm/scale_fact

    sclgrm = sclgrm ** 0.5

    x, y = np.meshgrid(t, np.logspace(np.log10(fmin), np.log10(fmax), Ny))
    # x, y = np.meshgrid(t, np.linspace(fmin, fmax, Ny))
    im = ax.pcolormesh(x, y, sclgrm, cmap='jet', vmin=0, vmax=1)
    # im = ax.pcolormesh(x, y, sclgrm, cmap='inferno', vmin=0, vmax=1)
    ax.xaxis.set_tick_params(labelbottom=False)
    ax.set_xlim(0, max(t))
    ax.set_ylim(fmin, fmax)

    if s1s2s4:
        s1_1 = s1s2s4[0]
        s1_2 = s1s2s4[1]
        s2_1 = s1s2s4[2]
        s2_2 = s1s2s4[3]
        s4_1 = s1s2s4[4]
        s4_2 = s1s2s4[5]
        f_top = s1s2s4[6]
        plt.plot([s1_1, s1_1], [5, 500], color='w')
        plt.plot([s1_2, s1_2], [5, 500], color='w')
        plt.plot([s2_1, s2_1], [5, 500], color='w')
        plt.plot([s2_2, s2_2], [5, 500], color='w')
        plt.plot([s4_1, s4_1], [5, 500], color='w')
        plt.plot([s4_2, s4_2], [5, 500], color='w')
        plt.plot([0, max(t)], [f_top, f_top], color='w')

    cursor = Cursor(ax, useblit=True, color='red', linewidth=1)

    ax.set_title(title)
    ax.set_ylabel('Frequency (Hz)', color='w')
    lev = np.linspace(0, 1, 10, endpoint=True)
    # ax.contour(x, y, sclgrm, levels=lev, colors='w', linewidths=1)
    ax.set_yscale('log')

    # ax.grid(True)
    # cursor = Cursor(ax, color='r', lw=1)

    divider = make_axes_locatable(ax)
    ax_top = divider.append_axes("top", 1.2, pad=0.1, sharex=ax)
    ax_bott = divider.append_axes("bottom", 1.2, pad=0.1, sharex=ax)
    ax_right = divider.append_axes("right", 1.2, pad=0.1, sharey=ax)

    # make some labels invisible
    # ax_bott.xaxis.set_tick_params(labelbottom=False)
    ax_right.yaxis.set_tick_params(labelleft=False)
    ax_top.xaxis.set_tick_params(labelbottom=False)

    s_t = np.zeros(Nx)
    s_f = np.zeros(Ny)
    for i in range(Nx):
        s_t[i] = np.sum(sclgrm[:, i])
    for i in range(Ny):
        s_f[i] = np.sum(sclgrm[i, :])

    ax_top.plot(t, sig)
    ax_top.set_title(title)
    ax_bott.plot(t, s_t)
    ax_bott.set_xlim(0, max(t))
    ax_bott.set_xlabel('Normalized Time (sec)', color='w')
    ax_bott.grid(True)
    ax_right.plot(s_f, f)
    ax_right.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
    ax_right.set_ylim(fmin, fmax)
    ax_right.grid(True)
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    if file_png:
        plt.savefig(file_png)
    plt.show()



def wpt_lev_data(sig, lev, n_bands, wavelet, boundary_mode='constant'):
    """ Compute WPT 2D array of WPT coefficients for given signal, wavelet family
        and decomposition level
        Returns: wpt data - 2D arrays with node data
                 nodes - node labels for the level
    """
    nodes = []
    # levels = np.linspace(1, maxlev, maxlev, dtype='int')
    w = pywt.Wavelet(wavelet)
    wp = pywt.WaveletPacket(data=sig, wavelet=w, mode=boundary_mode)
    node_lbl_lev = wp.get_level(lev, 'freq')

    for j in range(n_bands):
        nodes.append(node_lbl_lev[j].path)

    n_samp = len(wp[node_lbl_lev[0].path].data)
    wpt = np.zeros((n_bands, n_samp))
    for i in range(n_bands):
        lb = nodes[i]
        wpt[i, :] = wp[lb].data
    return wpt, nodes


def invert_wpt_sig(wpt, basis_lbls, n1, n2, wavelet_type, mode_type='constant'):
    """ invert signal using FULL best basis provided by labels"""
    wp_new = pywt.WaveletPacket(data=None, wavelet=wavelet_type, mode=mode_type)
    for i in range(len(basis_lbls)):
        lb = basis_lbls[i]
        wpt_data = np.zeros(wpt.shape[1], dtype=np.float32)
        wpt_data[n1:n2] = wpt[i, n1:n2]
        wp_new[lb] = wpt_data
    sig_recon = wp_new.reconstruct(update=False)
    return sig_recon
