import numpy as np
from numpy import correlate
# import matplotlib
# matplotlib.use('Qt5Agg')
# import matplotlib.pyplot as plt

def quality_test(stack, max_angle=45, max_t_prc=20):
    """ Test beat quality using angle from median and beat duration variance """
    # optional beat alignment, but it may not work all the time
    # stack, _ = adjustSyncS2(stack, s2_start, s2_end)
    # plot_stack(stack, FS, 0, 0, title='Pat: ' + patID + ', CH: ' + ch)

    beat_med = np.median(stack, axis=0)
    # plt.plot(beat_med)
    # plt.show()

    nb = stack.shape[0]
    ha_len = []
    beat_ang = np.zeros(nb)
    for i in range(nb):
        x = stack[i]
        beat_len = len(np.trim_zeros(x, 'b'))
        ha_len.append(beat_len)
        # distance between two vectors
        nrm = np.linalg.norm(x - beat_med)
        # angle between two vectors
        ang = np.dot(x, beat_med) / (np.linalg.norm(x) * np.linalg.norm(beat_med))
        ang = np.arccos(ang)
        ang = np.degrees(ang)
        beat_ang[i] = ang

        # print('Beat:', i, 'Dist:', np.round(nrm, 3), 'Beat len:', beat_len, 'Angle:', np.round(ang, 3))

    large_ang = np.where(abs(beat_ang) > max_angle)[0]

    ha_len = np.array(ha_len, dtype='int')
    ha_med = np.median(ha_len)
    # print('Length difference from median:', ha_med)
    prc_len = np.zeros(nb)
    for i in range(nb):
        d = abs(ha_len[i] - ha_med)
        # d = (ha_len[i] - ha_med)**2

        prc = (d / ha_med) * 100
        prc_len[i] = prc
        # print('Beat:', i, 'Len:', ha_len[i], 'Dist:', d, 'Perc:', np.round(prc, 3))

    len_dev = np.where(prc_len > max_t_prc)[0]
    len_dev = set(len_dev)
    large_ang = set(large_ang)

    bad_beats = len_dev.union(large_ang)
    bad_beats = list(bad_beats)
    print(bad_beats)

    FS = 4000
    # plot_stack(stack, FS, 0, 0)

    if len(bad_beats) != 0:
        stack = np.delete(stack, bad_beats, axis=0)
        # plot_stack(stack, FS, 0, 0, title='Pat: ' + patID + ', CH: ' + ch)
    return stack, bad_beats


def quality_test_S1S2(stack, s1_1, s1_2, s2_1, s2_2, max_angle=60, max_t_prc=20, len_only=False):
    """ Test beat quality using angle from median and beat duration variance
        defaults max_angle=45, max_t_prc=20
        s1_1, s1_2, s2_1, s2_2 - np arrays for start and end indexes
        len_only - test for length variance only
    """
    # optional beat alignment, but it may not work all the time
    # stack, _ = adjustSyncS2(stack, s2_start, s2_end)
    # plot_stack(stack, FS, 0, 0, title='Pat: ' + patID + ', CH: ' + ch)

    beat_med = np.median(stack, axis=0)
    # plt.plot(beat_med)
    # plt.show()

    nb = stack.shape[0]
    ha_len = []
    beat_ang = np.zeros(nb)
    for i in range(nb):
        x = stack[i]
        beat_len = len(np.trim_zeros(x, 'b'))
        ha_len.append(beat_len)
        # distance between two vectors
        nrm = np.linalg.norm(x - beat_med)
        # angle between two vectors
        ang = np.dot(x, beat_med) / (np.linalg.norm(x) * np.linalg.norm(beat_med))
        ang = np.arccos(ang)
        ang = np.degrees(ang)
        beat_ang[i] = ang

        # print('Beat:', i, 'Dist:', np.round(nrm, 3), 'Beat len:', beat_len, 'Angle:', np.round(ang, 3))

    large_ang = np.where(abs(beat_ang) > max_angle)[0]

    ha_len = np.array(ha_len, dtype='int')
    ha_med = np.median(ha_len)
    # print('Length difference from median:', ha_med)
    prc_len = np.zeros(nb)
    for i in range(nb):
        d = abs(ha_len[i] - ha_med)
        # d = (ha_len[i] - ha_med)**2

        prc = (d / ha_med) * 100
        prc_len[i] = prc
        # print('Beat:', i, 'Len:', ha_len[i], 'Dist:', d, 'Perc:', np.round(prc, 3))

    len_dev = np.where(prc_len > max_t_prc)[0]

    len_dev = set(len_dev)
    large_ang = set(large_ang)
    if len_only:
        bad_beats = len_dev
        bad_beats = list(bad_beats)
    else:
        bad_beats = len_dev.union(large_ang)
        bad_beats = list(bad_beats)

    # print('Stack shape:', stack.shape)
    # print('Bad beats:', bad_beats)
    # print('Num of bad', len(bad_beats))

    # FS = 4000
    # plot_stack_s1s2_v2(stack, FS, s1_1, s1_2, s2_1, s2_2)

    if len(bad_beats) == nb:
        raise ValueError('### No good beats left after quality test ###')

    if len(bad_beats) != 0:
        stack = np.delete(stack, bad_beats, axis=0)
        s1_1 = np.delete(s1_1, bad_beats)
        s1_2 = np.delete(s1_2, bad_beats)
        s2_1 = np.delete(s2_1, bad_beats)
        s2_2 = np.delete(s2_2, bad_beats)
        # plot_stack_s1s2_v2(stack, FS, s1_1, s1_2, s2_1, s2_2)

    bad_s1s2 = s1s2_pos(s1_1, s1_2, s2_1, s2_2)

    # print('Bad s1s2')
    # print(bad_s1s2)

    if len(bad_s1s2) != 0:
        stack = np.delete(stack, bad_s1s2, axis=0)
        s1_1 = np.delete(s1_1, bad_s1s2)
        s1_2 = np.delete(s1_2, bad_s1s2)
        s2_1 = np.delete(s2_1, bad_s1s2)
        s2_2 = np.delete(s2_2, bad_s1s2)
        bad_beats.append(bad_s1s2)

    # print(bad_beats)
    # print('Stack size:', len(stack))

    if len(stack) == 0:
        raise ValueError('### No good beats left after quality test ###')

    return stack, bad_beats, s1_1, s1_2, s2_1, s2_2


def quality_test_v2(stack, s1_1, s1_2, s2_1, s2_2, max_t_prc=20):
    """ Quality test ver.2. Finds beats with length out of ordinary """
    nb = stack.shape[0]
    beat_med = np.median(stack, axis=0)
    ha_len = []
    for i in range(nb):
        x = stack[i]
        beat_len = len(np.trim_zeros(x, 'b'))
        ha_len.append(beat_len)

    ha_len = np.array(ha_len, dtype='int')
    ha_med = np.median(ha_len)

    prc_len = np.zeros(nb)
    for i in range(nb):
        d = abs(ha_len[i] - ha_med)
        # d = (ha_len[i] - ha_med)**2
        prc_len[i] = (d / ha_med) * 100
        # print('Beat:', i, 'Len:', ha_len[i], 'Dist:', d, 'Perc:', np.round(prc, 3))

    len_dev = np.where(prc_len > max_t_prc)[0]
    bad_beats = list(set(len_dev))

    print('Beats do not fit length deviation threshold')
    print(bad_beats)

    if len(bad_beats) != 0:
        stack = np.delete(stack, bad_beats, axis=0)
        s1_1 = np.delete(s1_1, bad_beats)
        s1_2 = np.delete(s1_2, bad_beats)
        s2_1 = np.delete(s2_1, bad_beats)
        s2_2 = np.delete(s2_2, bad_beats)

    if len(stack) == 0:
        raise ValueError('### No good beats left after quality test ###')

    return stack, bad_beats, s1_1, s1_2, s2_1, s2_2


def s1s2_pos(s1_1, s1_2, s2_1, s2_2):
    """ Test position of s1 and s2 indexes with respect to median and remove outliers """
    s1_1_med = np.median(np.array(s1_1))
    s1_2_med = np.median(np.array(s1_2))
    s1_peak = 0.5 * (np.array(s1_1) + np.array(s1_2))
    s1_peak_med = np.median(s1_peak)

    s2_1_med = np.median(np.array(s2_1))
    s2_2_med = np.median(np.array(s2_2))
    s2_peak = 0.5 * (np.array(s2_1) + np.array(s2_2))
    s2_peak_med = np.median(s2_peak)

    bad_beats = []
    # check S2 peak position
    for i in range(len(s2_1)):
        d = abs(s2_peak[i] - s2_peak_med)
        # print(d/s2_peak_med, 0.1*s2_peak_med)
        if d > 0.05*s2_peak_med:
            bad_beats.append(i)

    for i in range(len(s1_2)):
        d1 = abs(s1_1[i] - s1_1_med)
        d2 = abs(s1_2[i] - s1_2_med)
        if (d1 > 0.2*s1_1_med) & (d2 > 0.2*s1_2_med):
            bad_beats.append(i)
        else:
            if (d2 > 0.2*s1_2_med) & (d1 <= 0.2*s1_1_med):
                s1_2[i] = s1_2_med
    bad_beats = list(set(bad_beats))
    return bad_beats


def adjustSyncS2(stack, s2_start, s2_end):
    """ Align beat stack with respect the ref beat nearest to median.
        Use cross-correlation with reference beat.
        Output: stack of aligned beats, same stack size
    """
    nbeats = np.size(stack, axis=0)
    s2_peak = np.zeros(nbeats)
    for i in range(nbeats):
        s2_peak[i] = 0.5*(s2_start[i] + s2_end[i])

    n1 = int(np.median(s2_start))
    n2 = int(np.median(s2_end))
    stackS2 = stack[:, n1:n2]
    peak_med = np.median(s2_peak)

    dist_from_med = np.abs(s2_peak - peak_med)
    ind_min = np.where(dist_from_med == np.min(dist_from_med))[0][0]

    ref = stackS2[ind_min, :]
    ref = ref / np.max(np.abs(ref))

    zref = np.where(ref < 0)[0]
    ref[zref] = 0
    ref = ref * np.hamming(len(ref))

    # cc = np.zeros((nbeats, (stackS2.shape[1]*2-1)))
    beat_shift = []

    for i in range(nbeats):
        s = stackS2[i]
        s = s - np.mean(s)
        s = s / np.max(np.abs(s))
        zz = np.where(s < 0)[0]
        s[zz] = 0
        s = s * np.hamming(len(s))

        # plt.plot(s)
        # plt.plot(ref, 'r')
        # plt.title('Beat:' + str(i))
        # plt.show()

        c = correlate(ref, s, mode='full')

        iz = np.where(c < 0)[0]
        c[iz] = 0

        # plt.plot(c)
        # plt.title('Beat:' + str(i))
        # plt.grid(True)
        # plt.show()

        ind = np.argmax(c)
        sh = ind - len(ref) + 1

        beat = stack[i]
        beat_sh = np.roll(beat, sh)
        beat_shift.append(sh)

        if sh < 0:
            beat_sh[sh:] = 0

        else:
            beat_sh[0:sh] = 0
        stack[i, :] = beat_sh

    # for i in range(nbeats):
    #     plt.plot(cc[i])
    # plt.show()
    return stack, beat_shift


# def plot_stack(stack, fs, s2_start_ind, s2_end_ind, title='', markers=True):
#     NORM_BEATS = True
#     BEAT_SCALE = 1
#     BEAT_OFFSET = 1
#
#     x = np.arange(stack.shape[1]) / fs
#     nb_all = stack.shape[0]
#     for j in range(nb_all):
#         y = stack[j]
#         if NORM_BEATS:
#             y = y / np.max(abs(y))
#         else:
#             y = y / BEAT_SCALE
#         plt.plot(x, y + j * BEAT_OFFSET)
#         # draw vertical lines for S2 start and S2 end point
#         if markers:
#             plt.plot([s2_start_ind / fs, s2_start_ind / fs], [-0.5 * BEAT_OFFSET, nb_all * BEAT_OFFSET + 2], color='b')
#             plt.plot([s2_end_ind / fs, s2_end_ind / fs], [-0.5 * BEAT_OFFSET, nb_all * BEAT_OFFSET + 2], color='lime')
#             # plt.plot([(s2_start_ind+1024)/fs, (s2_start_ind+1024)/fs], [-0.5*BEAT_OFFSET, nb_all*BEAT_OFFSET+2],
#             #          color='lime')
#     figManager = plt.get_current_fig_manager()
#     figManager.window.showMaximized()
#     plt.grid(True)
#     plt.title(title)
#     plt.show()
#
#
# def plot_stack_s1s2_v2(stack, fs, s1_start, s1_end, s2_start, s2_end, title='', markers=True, file_png=None):
#     """ Shor S1 and S2 boundary markings for each beat"""
#     NORM_BEATS = True
#     BEAT_SCALE = 1
#     BEAT_OFFSET = 1
#
#     x = np.arange(stack.shape[1]) / fs
#     nb_all = stack.shape[0]
#     for j in range(nb_all):
#         y = stack[j]
#         if NORM_BEATS:
#             y = y / np.max(y)
#         else:
#             y = y / BEAT_SCALE
#         plt.plot(x, y + j * BEAT_OFFSET)
#         # draw vertical lines for S2 start and S2 end point
#         if markers:
#             plt.plot([s1_start[j] / fs, s1_start[j] / fs], [j*BEAT_OFFSET - 0.25, j*BEAT_OFFSET + 0.25], color='r')
#             plt.plot([s1_end[j] / fs, s1_end[j] / fs], [j * BEAT_OFFSET - 0.25, j * BEAT_OFFSET + 0.25], color='r')
#             plt.plot([s2_start[j] / fs, s2_start[j] / fs], [j * BEAT_OFFSET - 0.25, j*BEAT_OFFSET + 0.25], color='lime')
#             plt.plot([s2_end[j] / fs, s2_end[j] / fs], [j * BEAT_OFFSET - 0.25, j * BEAT_OFFSET + 0.25], color='lime')
#             # plt.plot([(s2_start_ind+1024)/fs, (s2_start_ind+1024)/fs], [-0.5*BEAT_OFFSET, nb_all*BEAT_OFFSET+2],
#             #          color='lime')
#     figManager = plt.get_current_fig_manager()
#     figManager.window.showMaximized()
#     plt.grid(True)
#     plt.title(title)
#     if file_png:
#         plt.savefig(file_png)
#     plt.show()
