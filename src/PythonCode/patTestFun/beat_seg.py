""" Beat segmentation code for patient breat hold records """

import numpy as np
from scipy.signal import savgol_filter, butter
from scipy.signal import cheby1, filtfilt
from SegECGh5 import SegECGh5
#import matplotlib
#matplotlib.use('Qt5Agg')
#import matplotlib.pyplot as plt


def beat_seg(mrn, bhs, fs, bh_stat, CHANNELS, sg_tw=0):
    """
    Beat segmentation and breath hold combination using bhs dict from h5 file
    :param mrn: mrn
    :param bhs: dict read from h5 for mrn
    :param fs: sampling frequency
    :param bh_stat: list of breath hold status codes
    :CHANNELS: list of channels to use
    :param sg_tw: time constant (ms) for SG filter, if needed
    :return: list of beat stacks for 6 channels, all breath holds
    """
    # Design low-pass filter for signals
    fc = 500  # 500 Hz cut-off for signal LPF
    wc = fc / (fs / 2)
    b_lpf, a_lpf = butter(6, wc, btype='low')

    # process each breath hold segment
    stacks_all_bh = []  # it is list of bh, list of channels, list of filtered stacks
    # indexes combined for all bh
    s1_start_all_bh = []
    s1_end_all_bh = []
    s1_peak_all_bh = []
    s2_start_all_bh = []
    s2_end_all_bh = []
    s2_peak_all_bh = []
    rr_beats_all_bh = []

    n_bhs = len(bhs)
    count_bh = 1
    for bh in bhs:
        # print('BH#:', count_bh, 'out of', len(bhs))
        bh_ind = count_bh - 1
        data = bhs[bh]
        data = data.T

        # check bh status key
        bh_status = bh_stat[bh_ind]
        if bh_status == 4:
            print('Breath hold', bh, 'status:', bh_status, 'Skipping ...')
            count_bh += 1
            continue

        ecg = data[7]

        # check if ECG is Ok
        ecg_status = test_ECG(ecg, fs)
        if ecg_status is False:
            print('Breath hold', bh, 'has no ECG. Skipping ...')
            # test_ECG(ecg, a, b, show=True)
            count_bh += 1
            continue

        try:
            rr_beats, s1_start_all, s1_end_all, s2_start_all, s2_end_all, s1_peak_ix_all, s2_peak_ix_all, ecg, \
            _ = SegECGh5(data, fs, CHANNELS)
        except Exception as e:
            print('Patient:', mrn, 'there was SegECGh5 problem ...')
            print(e)
            count_bh += 1
            continue

        # indexes for index arrays: [i][j][k] --> i - breath hold, j - channel, k - beat index
        s1_start_all_bh.append(s1_start_all)
        s1_end_all_bh.append(s1_end_all)
        s2_start_all_bh.append(s2_start_all)
        s2_end_all_bh.append(s2_end_all)
        s1_peak_all_bh.append(s1_peak_ix_all)
        s2_peak_all_bh.append(s2_peak_ix_all)
        rr_beats_all_bh.append(rr_beats)

        nbeats = len(rr_beats)

        # find max length of the beat
        tmp = np.zeros(nbeats, dtype='int')
        i = 0
        for tp in rr_beats:
            tmp[i] = tp[1] - tp[0]
            i += 1
        max_beat_len = np.max(tmp)

        stacks_all_channels = []

        for ch in CHANNELS:
            # print('Channel:', ch)
            ch_ind = int(ch) - 1
            # extract and filter channel of interest
            sig_ch = data[ch_ind, :]
            # low-pass filter applied
            sig_ch = filtfilt(b_lpf, a_lpf, sig_ch, method="gust")

            stack_ch = np.zeros((nbeats, max_beat_len))

            if sg_tw != 0:
                Nw = int(np.ceil(sg_tw * fs))
                if Nw % 2 == 0:
                    Nw += 1
                sg_order = 3
                sig_smooth = savgol_filter(sig_ch, Nw, sg_order, mode='constant')
                sig_sg = sig_ch - sig_smooth
            else:
                sig_sg = sig_ch

            for j in range(nbeats):
                beat_start = rr_beats[j][0]
                beat_end = rr_beats[j][1]
                beat_len = beat_end - beat_start
                stack_ch[j, 0:beat_len] = sig_sg[beat_start:beat_end]

            stacks_all_channels.append(stack_ch)
        stacks_all_bh.append(stacks_all_channels)

        # for ch in CHANNELS:
        #     i = int(ch) - 1
        #     st = stacks_all_channels[i]
        #     s1_1 = s1_start_all_bh[count_bh-1][i]
        #     s1_2 = s1_end_all_bh[count_bh-1][i]
        #     s2_1 = s2_start_all_bh[count_bh-1][i]
        #     s2_2 = s2_end_all_bh[count_bh-1][i]
        #
        #     tit = 'Channel: ' + ch
        #     plot_stack_s1s2_v2(st, fs, s1_1, s1_2, s2_1, s2_2, title=tit)
        count_bh += 1

    # update number of breath holds since some could be bad
    n_bhs = len(stacks_all_bh)

    if n_bhs == 0:
        print('Patient has no good breath holds. Signal of poor quality.')
        exit(0)

    # compute beat rate statistics
    t_beat = []
    for ii in range(n_bhs):
        rr = rr_beats_all_bh[ii]
        for n in range(len(rr)):
            tb = (rr[n][1] - rr[n][0]) / fs
            t_beat.append(tb)
    t_beat = np.array(t_beat)
    rr_mean = np.mean(t_beat)
    rr_var = np.var(t_beat)

    rr_stat = {'mean': rr_mean, 'rr_var': rr_var}

    # create list of all bh for each channel.
    stacks_list = []  # list of lists of channels and filters
    for i in range(len(CHANNELS)):
        # filtered_list = []  # list of channels and filters combined over bh
        # for j in range(len(FILTERS)):
        tmp_list = []
        for k in range(n_bhs):
            st = stacks_all_bh[k][i]
            tmp_list.append(st)
        st_combined = combine_bh_stacks2(tmp_list)
        stacks_list.append(st_combined)

    # combined indexes for all bh, indexes here: [i][j] --> i - channel, j - beat index
    s1_start_list = []
    s1_end_list = []
    s1_peak_list = []
    s2_start_list = []
    s2_end_list = []
    s2_peak_list = []
    for i in range(len(CHANNELS)):
        s1_start_tmp = []
        s1_end_tmp = []
        s1_peak_tmp = []
        s2_start_tmp = []
        s2_end_tmp = []
        s2_peak_tmp = []
        for j in range(n_bhs):
            s1_start_data = s1_start_all_bh[j][i]
            s1_start_tmp = s1_start_tmp + s1_start_data

            s1_end_data = s1_end_all_bh[j][i]
            s1_end_tmp = s1_end_tmp + s1_end_data

            s1_peak_data = s1_peak_all_bh[j][i]
            s1_peak_tmp = s1_peak_tmp + s1_peak_data

            s2_start_data = s2_start_all_bh[j][i]
            s2_start_tmp = s2_start_tmp + s2_start_data

            s2_end_data = s2_end_all_bh[j][i]
            s2_end_tmp = s2_end_tmp + s2_end_data

            s2_peak_data = s2_peak_all_bh[j][i]
            s2_peak_tmp = s2_peak_tmp + s2_peak_data

        s1_start_list.append(s1_start_tmp)
        s1_end_list.append(s1_end_tmp)
        s1_peak_list.append(s1_peak_tmp)
        s2_start_list.append(s2_start_tmp)
        s2_end_list.append(s2_end_tmp)
        s2_peak_list.append(s2_peak_tmp)


    # # # check beat stack for each channel
    # for i in range(len(CHANNELS)):
    #     ch = CHANNELS[i]
    #     st = stacks_list[i]
    #     s1_1 = s1_start_list[i]
    #     s1_2 = s1_end_list[i]
    #     s2_1 = s2_start_list[i]
    #     s2_2 = s2_end_list[i]
    #
    #     tit = 'Channel: ' + str(ch)
    #     plot_stack_s1s2_v2(st, fs, s1_1, s1_2, s2_1, s2_2, title=tit)


    data_info_dict = {'CHANNELS': CHANNELS,
                      'N_BH': n_bhs,
                      'FS': fs,
                      'LPF_TYPE': 'Butterworth', 'LPF_FC': fc,
                      'T_RR': rr_mean, 'T_RR_VAR': rr_var,
                      'S1_START': s1_start_list,
                      'S1_END': s1_end_list,
                      'S1_PEAK': s1_peak_list,
                      'S2_START': s2_start_list,
                      'S2_END': s2_end_list,
                      'S2_PEAK': s2_peak_list}

    return stacks_list, data_info_dict


def test_ECG(ecg_data, fs):
    """ check if ECG is present """
    # design filter for ECG
    fc = 40.
    wc = fc / (fs / 2)
    b, a = cheby1(4, 3, wc, 'low')

    ecg_data = ecg_data - np.mean(ecg_data)
    ecg_filt = filtfilt(b, a, ecg_data, method='gust')

    ecg = ecg_filt*10
    ecg_status = True
    # print('ECG swing:', np.round(np.max(ecg) - np.min(ecg), 2))
    if (np.max(ecg) - np.min(ecg)) < 350/32000:
        ecg_status = False
    return ecg_status


def combine_bh_stacks2(stacks_list):
    """ Combine stacks from list """
    n_bh = len(stacks_list)
    # find max length over all BH
    ha_len_bh = np.zeros(n_bh, dtype='int')     # max length per BH stack
    nb_bh = np.zeros(n_bh, dtype='int')         # number of beats per BH
    for i in range(n_bh):
        stack_bh = stacks_list[i]
        nb_bh[i] = stack_bh.shape[0]
        ha_len_bh[i] = stack_bh.shape[1]
    # max len of all BH stacks
    ha_max_len = np.max(ha_len_bh)

    nbeats = np.sum(nb_bh)
    stack = np.zeros((nbeats, ha_max_len))

    # combine individual BH stacks
    n_curr = 0  # counter of current number of beats
    for i in range(n_bh):
        stack_bh = stacks_list[i]
        stack_nb = stack_bh.shape[0]
        stack_len = stack_bh.shape[1]
        stack[n_curr:(n_curr + stack_nb), 0:stack_len] = stack_bh
        n_curr += stack_nb  # update beat counter
    return stack


def plot_stack_s1s2_v2(stack, fs, s1_start, s1_end, s2_start, s2_end, title='', markers=True, file_png=None):
    """ Shor S1 and S2 boundary markings for each beat"""
    NORM_BEATS = True
    BEAT_SCALE = 1
    BEAT_OFFSET = 1

    x = np.arange(stack.shape[1]) / fs
    nb_all = stack.shape[0]
    for j in range(nb_all):
        y = stack[j]
        if NORM_BEATS:
            y = y / np.max(y)
        else:
            y = y / BEAT_SCALE
        plt.plot(x, y + j * BEAT_OFFSET)
        # draw vertical lines for S2 start and S2 end point
        if markers:
            plt.plot([s1_start[j] / fs, s1_start[j] / fs], [j*BEAT_OFFSET - 0.25, j*BEAT_OFFSET + 0.25], color='r')
            plt.plot([s1_end[j] / fs, s1_end[j] / fs], [j * BEAT_OFFSET - 0.25, j * BEAT_OFFSET + 0.25], color='r')
            plt.plot([s2_start[j] / fs, s2_start[j] / fs], [j * BEAT_OFFSET - 0.25, j*BEAT_OFFSET + 0.25], color='lime')
            plt.plot([s2_end[j] / fs, s2_end[j] / fs], [j * BEAT_OFFSET - 0.25, j * BEAT_OFFSET + 0.25], color='lime')
            # plt.plot([(s2_start_ind+1024)/fs, (s2_start_ind+1024)/fs], [-0.5*BEAT_OFFSET, nb_all*BEAT_OFFSET+2],
            #          color='lime')
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    plt.grid(True)
    plt.title(title)
    if file_png:
        plt.savefig(file_png)
    plt.show()
    # # automatic closing of plot
    # plt.show(block=False)
    # plt.pause(3)
    # plt.close()
