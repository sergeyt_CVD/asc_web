import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.use('Qt5Agg')


def plot_stack_s1s2_v2(stack, fs, s1_start, s1_end, s2_start, s2_end, title='', markers=True, file_png=None):
    """ Shor S1 and S2 boundary markings for each beat"""
    NORM_BEATS = True
    BEAT_SCALE = 1
    BEAT_OFFSET = 1

    x = np.arange(stack.shape[1]) / fs
    nb_all = stack.shape[0]
    for j in range(nb_all):
        y = stack[j]
        if NORM_BEATS:
            y = y / np.max(y)
        else:
            y = y / BEAT_SCALE
        plt.plot(x, y + j * BEAT_OFFSET)
        # draw vertical lines for S2 start and S2 end point
        if markers:
            plt.plot([s1_start[j] / fs, s1_start[j] / fs], [j*BEAT_OFFSET - 0.25, j*BEAT_OFFSET + 0.25], color='r')
            plt.plot([s1_end[j] / fs, s1_end[j] / fs], [j * BEAT_OFFSET - 0.25, j * BEAT_OFFSET + 0.25], color='r')
            plt.plot([s2_start[j] / fs, s2_start[j] / fs], [j * BEAT_OFFSET - 0.25, j*BEAT_OFFSET + 0.25], color='lime')
            plt.plot([s2_end[j] / fs, s2_end[j] / fs], [j * BEAT_OFFSET - 0.25, j * BEAT_OFFSET + 0.25], color='lime')
            # plt.plot([(s2_start_ind+1024)/fs, (s2_start_ind+1024)/fs], [-0.5*BEAT_OFFSET, nb_all*BEAT_OFFSET+2],
            #          color='lime')
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    plt.grid(True)
    plt.title(title)
    if file_png:
        plt.savefig(file_png)
    plt.show()
    # # automatic closing of plot
    # plt.show(block=False)
    # plt.pause(3)
    # plt.close()
