"""
Code to produce segmentation of ECG channel and multiple PCG channels
for a single breath hold.

"""

import os, sys
import numpy as np
from numpy import polyfit, poly1d
import peakutils, yaml
from scipy import signal
from scipy.signal import cheby1, cheby2, filtfilt
from scipy.signal import savgol_filter
from scipy.signal import decimate
from numpy.lib import stride_tricks
# import matplotlib
# matplotlib.use('Qt5Agg')
# import matplotlib.pyplot as plt
from pathlib import Path
from segment_ecg import segment_ecg
# from ecg_peaks_st import ecg_peaks_st

DISPLAY = False
EPS = 1e-9


def read_config(config_file):
    """ Read parameters from config file """
    with open(config_file, 'r') as fl:
        root_dict = yaml.safe_load(fl)

    PARAMS_DICT = root_dict['SegECG']
    return PARAMS_DICT


def SegECGh5(data, fs, channels):
    """ Segment recording using ECG data from channel 8.
        data - 2D array of all 8 channels
        channels - list of channels to process, strings
        Return: List of 2D beat stacks
    """

    PROJ_DIR = Path(__file__).parent.parent.parent
    # config_file = str(PROJ_DIR) + '/Libs/config.yml'
    config_file = str(PROJ_DIR) + '/PythonCode/patTestFun/Config/config.yml'

    params = read_config(config_file)
    ecg_off = params['ECG_OFFSET']                  # ecg starting point offset
    ecg_decim = params['ECG_DECIM']                 # decimation factor for ecg seg processing
    ecg_gain = params['ECG_GAIN']                   # scaling ECG signal
    cheby_size = params['CHEBY_SIZE']               # Cheby1 filter size for ECG LPF
    cheby_order = params['CHEBY_ORDER']             # Cheby1 filter order, ECG LPF
    cheby_fc = params['CHEBY_FC']                   # ECG LPF cut-off
    ecg_channel = params['ECG_CHANNEL']             # ECG channel number counting from 0
    sg_order = params['SG_ORDER']                   # Sav-Gol filter order
    sg_base_tw = params['SG_BASELINE_WIN']          # Win size in sec for baseline stabilization
    ecg_p_width = params['ECG_PEAK_WIDTH']
    sh_power = params['SHANN_POW']
    ecg_low_th = params['ECG_PEAK_LOW_TH']          # peak thresh used for outlier detection
    ecg_min_peaks = params['ECG_MIN_PEAK_NUM']      # min number of ECG should be detected
    sg_tw = params['SG_SIG_TW']                     # Sav-Golay window for signal HPF
    sh_w2 = params['SHANN_W2']                      # Shannon operator win size
    en_tw = params['EN_SMOOTH_TW']                  # Win size for energy curve smoothing
    t_peak_min1 = params['MIN_DIST1']               # min distance for peak detection
    en_peak_off = params['EN_PEAK_OFFSET']          # energy peak offset
    thresh1 = params['THRESH1']                     # threshold1 for for energy curve fitting
    thresh2 = params['THRESH2']                     # threshold2 for energy curve fitting
    s2_off = params['S2_OFFSET']                    # S2 offset to cover S2 signal
    thresh_s2 = params['S2_THRESH']                 # threshold for S2 peak
    bias_shift = params['BIAS_SHIFT']               # Shift S2 start to compensate bias


    # process ECG channel first
    ecg = data[ecg_channel] * ecg_gain

    # Low-pass filter for ECG data
    fc = cheby_fc
    wc = fc / (fs / 2)
    rs = 40
    b, a = cheby2(cheby_order, rs, wc, 'low')
    # b, a = cheby1(cheby_size, cheby_order, wc, 'low')
    # w, h = signal.freqz(b, a)
    # freq = w * fs / (2 * np.pi)
    # plt.plot(freq, 20*np.log10(abs(h)))
    # plt.show()

    # filter ECG signal
    ecg = filtfilt(b, a, ecg, method='gust')

    # remove baseline wandering and DC offset
    Nsg = int(fs * sg_base_tw)
    if Nsg % 2 == 0:
        Nsg += 1
    ecg_smooth = savgol_filter(ecg, Nsg, sg_order)
    ecg = ecg - ecg_smooth

    # Test of ECG polarity
    Ntest = int(2 * fs)  # 2 sec interval
    xx_ecg = np.append(ecg, np.zeros(Ntest))
    stride = int(Ntest / 2)
    n_test = int(len(xx_ecg) / stride)

    test_seg = stride_tricks.as_strided(ecg, shape=(n_test, stride),
                                        strides=(ecg.strides[0] * stride, ecg.strides[0]))

    peak_p = np.zeros(n_test)
    peak_n = np.zeros(n_test)
    for i in range(n_test):
        peak_p[i] = np.max(test_seg[i])
        peak_n[i] = np.max(-test_seg[i])
    if np.median(peak_p) < np.median(peak_n):
        # print('ECG is inverted ...')
        ecg = -ecg

    # This is older code from SegECG for peak detection
    # rr_good = ecg_peaks_st(ecg, fs, 40, 1)
    # rr_stat = {'mean':0, 'var': 0}

    # Get RR peak indexes for heart beats
    rr_good, rr_failed, rr_stat = segment_ecg(ecg, int(fs))

    # print(len(rr_good))
    # print(len(rr_failed))
    # print(rr_stat)
    # exit(0)

    if DISPLAY:
        peak_idx = [v[0] for v in rr_good]
        y_peaks = ecg[peak_idx]
        plt.plot(ecg)
        plt.plot(peak_idx, y_peaks, 'ro')
        plt.title('ECG Peaks, Test #1')
        plt.show()

    nbeats = len(rr_good)
    if nbeats == 0:
        raise ValueError('No good beats were found ...')

    # find max length of the beat
    tmp = np.zeros(nbeats, dtype='int')
    i = 0
    for tp in rr_good:
        tmp[i] = tp[1] - tp[0]
        i += 1
    max_beat_len = np.max(tmp)

    # SG filter params
    Nw = int(np.ceil(sg_tw * fs))
    if Nw % 2 == 0:
        Nw += 1
    order = sg_order

    # get reference channel for alternative if channel data are bad. Use channel 4.
    sig_ref = data[3, :]
    # Ref channel
    sig_ref = sig_ref - savgol_filter(sig_ref, Nw, order, mode='constant')

    # This is lists of lists for indexes for all channels
    s1_start_all = []
    s1_end_all = []
    s2_start_all = []
    s2_end_all = []

    beat_start_all = []
    beat_end_all = []

    # indexes of S1 and S2 peaks for FILTERED signal
    s1_peak_ix_all = []
    s2_peak_ix_all = []

    for ch in channels:
        # print('SegECG CH:', ch)
        ch_ind = int(ch) - 1
        sig = data[ch_ind, :]

        sig = sig - savgol_filter(sig, Nw, order, mode='constant')

        if DISPLAY:
            peak_idx = [v[0] for v in rr_good]
            plt.plot(sig/max(abs(sig)))
            for i in range(len(peak_idx)):
                plt.plot([peak_idx[i], peak_idx[i]], [-1, 1], color='r')
            v0, v1 = rr_good[-1]
            plt.plot([v1, v1], [-1, 1], color='r')
            # plt.plot(ecg/max(abs(ecg)))
            plt.title('Signal ch: ' + str(ch) + ' and ECG peaks, Test #2')
            plt.show()

        # <----------- Find positions of S1 and S2 for each beat---------------->
        # Note that indexes will be with respect to beat start
        s1_start = []
        s1_end = []
        s2_start = []
        s2_end = []
        s1_peak_ix = []
        s2_peak_ix = []

        for i in range(nbeats):
            beat = np.zeros(max_beat_len)
            beat_ref = np.zeros(max_beat_len)
            tp = rr_good[i]
            ind1 = tp[0]
            ind2 = tp[1]

            beat_len = ind2 - ind1
            # Remove last 20% from peak search since S4 can be high
            end_off = int(beat_len * 0.2)
            ind2 = ind2 - end_off
            beat_len = ind2 - ind1
            beat[0:beat_len] = sig[ind1:ind2]
            u = (beat / np.max(abs(beat))) * 0.8
            nu = len(u)
            yu = u

            # Ref channel
            beat_ref[0:beat_len] = sig_ref[ind1:ind2]
            u_ref = (beat_ref / np.max(abs(beat_ref))) * 0.8
            yu_ref = u_ref

            # segment beat for Shannon operator
            nshift = 1
            Nw_sh = int(np.ceil(sh_w2 * fs))
            if Nw_sh % 2 != 0:
                Nw_sh += 1
            # append array from both ends
            Nww = int(Nw_sh / 2)
            u = np.append(np.zeros(Nww), u)
            u = np.append(u, np.zeros(Nww))
            beat_seg = stride_tricks.as_strided(u, shape=(nu, Nw_sh),
                                                strides=(u.strides[0] * nshift, u.strides[0]))

            # Ref channel
            u_ref = np.append(np.zeros(Nww), u_ref)
            u_ref = np.append(u_ref, np.zeros(Nww))
            beat_seg_ref = stride_tricks.as_strided(u_ref, shape=(nu, Nw_sh),
                                                    strides=(u_ref.strides[0] * nshift, u_ref.strides[0]))

            # energy array or rather Shannon transformed signal
            en = np.zeros(nu)
            en_ref = np.zeros(nu)
            n_seg = beat_seg.shape[0]
            # Apply 3d or 4th order Shannon operator to emphasize S1 and S2
            for j in range(n_seg):
                uu = beat_seg[j]
                pw = np.abs(uu) ** sh_power
                # pw = np.abs(uu) ** 2
                en[j] = -np.mean(np.log10(pw ** pw))

                # Ref channel
                uu = beat_seg_ref[j]
                pw = np.abs(uu) ** sh_power

                # print(uu, file=sys.stderr)
                # print('sh_power', file=sys.stderr)
                # print(sh_power, file=sys.stderr)

                # pw = np.abs(uu) ** 2
                en_ref[j] = -np.mean(np.log10(pw ** pw))

            # normalize and smooth resulting energy curve
            en = en / np.max(en)
            en_raw = en
            Nsg2 = int(np.ceil(en_tw * fs))
            if Nsg2 % 2 == 0:
                Nsg2 += 1
            en = savgol_filter(en, Nsg2, 2, mode='constant')

            # Ref channel
            en_ref = savgol_filter(en_ref, Nsg2, 2, mode='constant')

            # # plot energy curve and smoothed energy
            if DISPLAY:
                plt.plot(en_raw)
                plt.plot(en)
                plt.plot(abs(yu), color='m')
                plt.title('Test #6')
                plt.show()

            # fitted peaks curve for display
            en_fitted = np.zeros(len(en))
            peak_len = 0
            # peak_thresh = 0.5
            th_steps = [0.8, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.04, 0.03, 0.02, 0.01, 0.005,
                        0.004, 0.003, 0.002, 0.001, 5E-4, 0.0]

            # Find S1 & S2 peaks, adjust threshold if it fails to find two peaks
            ii = 0
            min_dist = int(t_peak_min1 * fs)
            ush_peaks_ix = None
            while peak_len < 2:
                peak_thresh = th_steps[ii]
                ush_peaks_ix = peakutils.indexes(en, thres=peak_thresh, min_dist=min_dist, thres_abs=True)
                peak_len = len(ush_peaks_ix)

                # print(peak_thresh, ush_peaks_ix)
                # plt.plot(en)
                # plt.plot([0, len(en)], [peak_thresh, peak_thresh], color='r')
                # plt.show()

                ii += 1
                # peak_thresh = th_steps[ii]
                if ii == len(th_steps):
                    print('Failed to find S1 and S2 peaks...')
                    print('Trying to use reference channel...')
                    # plt.plot(en)
                    # plt.plot([0, len(en)], [peak_thresh, peak_thresh], color='r')
                    # plt.show()
                    ii = 0
                    while peak_len < 2:
                        peak_thresh = th_steps[ii]
                        ush_peaks_ix = peakutils.indexes(en_ref, thres=peak_thresh, min_dist=min_dist, thres_abs=True)
                        peak_len = len(ush_peaks_ix)
                        ii += 1
                        if ii == len(th_steps):
                            print('Still unable to find S1 and S2 peaks ...')
                            raise Exception('Could not find S1 and S2!')

            # if peak_len > 2:
            #     print('Warning! For beat', i, 'found', peak_len, 'peaks for S1 and S2. Using the first two...')

            # plt.plot(en_raw)
            # plt.plot(en)
            # plt.plot(abs(yu), color='m')
            # plt.plot([0, len(en)], [peak_thresh, peak_thresh], color='b')
            # plt.title('Test #6-9')
            # plt.show()

            # print(ush_peaks_ix)

            # print('S1 and S2 peak positions:', ush_peaks_ix)

            # peak indexes and peak values
            s1peak_ix = ush_peaks_ix[0]
            s1peak = en[s1peak_ix]
            s2peak_ix = ush_peaks_ix[1]
            s2peak = en[s2peak_ix]

            # Fit energy curve above threshold by quadratic polynomial
            # set offset from peaks for curve fitting
            offset = int(np.ceil(en_peak_off * fs))
            # thresh = 0.05   # threshold of peak value, use data above thresh for fitting

            s1_tmp = en[0:(s1peak_ix + offset)]

            # ix_gr = np.where(s1_tmp > (s1peak*thresh))[0]

            ix_gr1 = np.where(s1_tmp > (s1peak * thresh1))[0]
            s1_left = ix_gr1[0]
            ix_gr2 = np.where(s1_tmp > (s1peak * thresh2))[0]
            s1_right = ix_gr2[-1]
            ix_gr = np.array([s1_left, s1peak_ix, s1_right])
            en_tmp = np.array([en[s1_left], en[s1peak_ix], en[s1_right]])

            off = int(np.ceil(s2_off * fs))
            x = np.linspace(0, s1peak_ix + off, s1peak_ix + off)
            z = polyfit(ix_gr, en_tmp, 2)
            p = poly1d(z)
            y1fit = p(x)

            # # plot fit for s1 with energy curves
            if DISPLAY:
                plt.plot(en_raw)
                plt.plot(en)
                plt.plot(x, y1fit, color='lime')
                plt.plot(ix_gr, en_tmp, 'o')
                plt.title('Test #3')
                plt.show()

            ix_tmp = np.where(y1fit > 0)[0]
            if len(ix_tmp) == 0:
                print('Could not find S1 limits. Exiting...')
                raise Exception('Failed to fid S1!')
            else:
                s1_left_ix = ix_tmp[0]
                s1_right_ix = ix_tmp[-1]

            # # Plot fitted S1 and energy curves
            if DISPLAY:
                en_fitted[s1_left_ix:(s1_right_ix + 1)] = y1fit[ix_tmp]
                plt.plot(en_raw)
                plt.plot(en)
                plt.plot(x, y1fit, color='lime')
                # plt.plot(en_fitted, color='lime')
                plt.plot([s1_left_ix, s1_left_ix], [-0.5, 0.5], color='b')
                plt.plot([s1_right_ix, s1_right_ix], [-0.5, 0.5], color='b')
                plt.title('Test #4')
                plt.show()

            # do the same for S2 peak
            thresh = thresh_s2
            s2_left = s2peak_ix - offset
            s2_tmp = en[s2_left:(s2peak_ix + offset)]
            ix2_gr = np.where(s2_tmp > (s2peak * thresh))[0]

            s2_lf = s2_left + ix2_gr[0]
            s2_rt = s2_left + ix2_gr[-1]

            # ix2_gr = ix2_gr + s2_left
            ix2_gr = np.array([s2_lf, s2peak_ix, s2_rt])
            en_tmp = np.array([en[s2_lf], en[s2peak_ix], en[s2_rt]])

            x2 = np.linspace(s2peak_ix - offset, s2peak_ix + offset, 2 * offset + 1)

            z = polyfit(ix2_gr, en_tmp, 2)
            p = poly1d(z)
            y2fit = p(x2)

            # # local fit for s2
            if DISPLAY:
                plt.plot(en_raw)
                plt.plot(en)
                plt.plot(x2, y2fit, color='lime')
                plt.plot(ix2_gr, en_tmp, 'o')
                plt.title('Test #5')
                plt.show()

            ix_tmp = np.where(y2fit > 0)[0]
            if len(ix_tmp) == 0:
                print('Could not find S2 limits. Exiting ...')
                raise Exception('Failed to find S2!')
            else:
                s2_left_ix = ix_tmp[0] + s2_left
                s2_right_ix = ix_tmp[-1] + s2_left

            # # plot energies and truncated fit for S1 and S2
            if DISPLAY:
                en_fitted[s2_left_ix:(s2_right_ix + 1)] = y2fit[ix_tmp]
                plt.plot(en_raw)
                plt.plot(en)
                plt.plot(x, y1fit, color='lime')
                plt.plot(x2, y2fit, color='lime')
                plt.plot([s1_left_ix, s1_left_ix], [-0.5, 0.5], color='b')
                plt.plot([s1_right_ix, s1_right_ix], [-0.5, 0.5], color='b')
                plt.plot([s2_left_ix, s2_left_ix], [-0.5, 0.5], color='b')
                plt.plot([s2_right_ix, s2_right_ix], [-0.5, 0.5], color='b')
                plt.title('Test #6')
                plt.show()

            if DISPLAY:
                t = np.arange(len(yu)) / fs
                plt.plot(t, en_raw)
                plt.plot(t, en)
                plt.plot(t, en_fitted, color='lime')
                plt.plot(t, yu, color='m')
                plt.plot([s1_left_ix / fs, s1_left_ix / fs], [-0.5, 0.5], color='b', linestyle=':')
                plt.plot([s1_right_ix / fs, s1_right_ix / fs], [-0.5, 0.5], color='b', linestyle=':')
                plt.plot([s2_left_ix / fs, s2_left_ix / fs], [-0.5, 0.5], color='b', linestyle=':')
                plt.plot([s2_right_ix / fs, s2_right_ix / fs], [-0.5, 0.5], color='b', linestyle=':')
                plt.title('Test #7')
                plt.grid(True)
                plt.show()

            s1_peak_ix.append(s1peak_ix)
            s2_peak_ix.append(s2peak_ix)

            # print('S1 Left:', s1_left_ix*decim/4000)
            # print('S1 Right:', s1_right_ix * decim / 4000)
            # print('S2 Left:', s2_left_ix * decim / 4000)
            # print('S2 Right:', s2_right_ix * decim / 4000)
            # print('|--------------------------------------------|')

            # shift right by 10 or 20 ms to correct bias
            n_shift = int(bias_shift * fs)
            s1_start.append(s1_left_ix + n_shift)
            s1_end.append(s1_right_ix + n_shift)
            s2_start.append(s2_left_ix + 2 * n_shift)
            s2_end.append(s2_right_ix + 2 * n_shift)

        s1_start_all.append(s1_start)
        s1_end_all.append(s1_end)
        s2_start_all.append(s2_start)
        s2_end_all.append(s2_end)
        s1_peak_ix_all.append(s1_peak_ix)
        s2_peak_ix_all.append(s2_peak_ix)
    return rr_good, s1_start_all, s1_end_all, s2_start_all, s2_end_all, s1_peak_ix_all, s2_peak_ix_all, ecg, \
        rr_stat




