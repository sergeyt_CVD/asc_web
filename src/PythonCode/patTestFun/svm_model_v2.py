""" This is copy of the svm_model.py used used in svm_wpt.py modified for production"""
import os
import numpy as np
import pandas as pd
# import matplotlib
# matplotlib.use('Qt5Agg')
# import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
import seaborn as sns
from sklearn.metrics import accuracy_score
from sklearn.utils import shuffle
from sklearn.metrics import roc_curve, roc_auc_score, precision_recall_curve

SAVE_RESULT = False


class SvmClf:
    def __init__(self, channels, columns):
        self.channels = channels  # list of channels to use
        self.columns = columns  # list of feature columns to use
        self.models = []
        self.proba_threshold = 0.65
        self.metrics = None
        self.perf = None

    def fit(self, df_train):
        """ Training models for selected channels.
            Input dataframe must have columns: MRN, TestKey, CH, Age, Sex, CAD, <feature columns>
            Note: CAD labels must be in [-1, 1]
        """
        df_train = df_train.dropna(subset=['CAD'])
        df_train['CAD'] = df_train['CAD'].astype('int')

        # make sure CAD labels in [-1, 1]
        cad_lbls = df_train.CAD.values
        for cc in cad_lbls:
            if cc not in [-1, 1]:
                raise ValueError('Error! Wrong CAD labels. Must be [-1, 1]')

        # create six SVM classifiers and one decision tree
        svm_clf_ch1 = SVC(kernel='linear', C=10, probability=True)
        svm_clf_ch2 = SVC(kernel='linear', C=10, probability=True)
        svm_clf_ch3 = SVC(kernel='linear', C=10, probability=True)
        svm_clf_ch4 = SVC(kernel='linear', C=10, probability=True)
        svm_clf_ch5 = SVC(kernel='linear', C=10, probability=True)
        svm_clf_ch6 = SVC(kernel='linear', C=10, probability=True)
        clf_all_ch = [(svm_clf_ch1, 1), (svm_clf_ch2, 2), (svm_clf_ch3, 3), (svm_clf_ch4, 4),
                      (svm_clf_ch5, 5), (svm_clf_ch6, 6)]

        # use classifiers only for specified channels
        clf2use = []
        for ch in self.channels:
            ch_ind = ch - 1
            clf2use.append(clf_all_ch[ch_ind])

        for clf_tup in clf2use:
            clf = clf_tup[0]
            ch = clf_tup[1]

            df_train_ch = df_train.loc[df_train['CH'] == ch]
            df_train_ch = shuffle(df_train_ch)

            X_train = df_train_ch.loc[:, self.columns].values
            X_train = X_train ** 2
            y_train = df_train_ch['CAD'].values

            # train classifier for each channel
            scl = StandardScaler()
            X_train_std = scl.fit_transform(X_train)
            clf.fit(X_train_std, y_train)
            self.models.append((scl, clf))

        # train decision tree for age feature
        df_train_unique = df_train.drop_duplicates(subset='TestKey')
        age_train = df_train_unique.loc[:, 'Age'].values
        age_train = age_train.reshape(-1, 1)
        y_train = df_train_unique['CAD'].values
        clf_tree = DecisionTreeClassifier(criterion='entropy', max_depth=3, random_state=0)
        clf_tree.fit(age_train, y_train)
        # Note last classifier will be decision tree with None as scaler
        self.models.append((None, clf_tree))

    def get_models(self):
        if len(self.models) != 0:
            return self.models
        else:
            raise ValueError('Error! There is no trained models ...')

    def validate(self, df_test, show_metrics=False, verbose=False):
        """ Test set of data using current model and test set
        """
        tk_test = df_test['TestKey'].values
        tk_test = list(set(tk_test))

        if len(self.models) == 0:
            raise ValueError('Error! Model needs to be trained first...')

        # print('Number of classifiers:', len(self.models))

        # make sure CAD labels in [-1, 1]
        cad_lbls = df_test.CAD.values
        for cc in cad_lbls:
            if cc not in [-1, 1]:
                raise ValueError('Error! Wrong CAD labels. Must be [-1, 1]')

        mrn_valid = []
        pat_age = []
        t_keys = []
        global_pred = []  # list of tuples (mrn, pred)
        pred_data = []  # list of predictions w/o mrn
        cad_truth = []
        mrn_proba_0 = []
        mrn_proba_1 = []
        for tk in tk_test:
            df_curr = df_test.loc[df_test['TestKey'] == tk]
            mrn = df_curr['MRN'].values[0]
            ch_avail = df_curr['CH'].values
            age = df_curr['Age'].values[0]
            pat_age.append(age)

            # check if patient has data channels for trained model
            for ch in self.channels:
                if ch not in ch_avail:
                    raise ValueError('Error! Patient does not have required channels')
                else:
                    pass

            mrn_valid.append(mrn)
            t_keys.append(tk)
            y_cad = df_curr['CAD'].values[0]
            cad_truth.append(y_cad)

            all_pred = []
            all_proba_0 = []    # mean probability of class -1
            all_proba_1 = []    # proba for class 1
            ind = 0

            # run predictions for specified channels
            for ch in self.channels:
                model = self.models[ind]
                scl = model[0]
                clf = model[1]

                row = df_curr.loc[df_curr['CH'] == ch]
                x_test = row.loc[:, self.columns].values
                x_test = x_test ** 2
                x_test_std = scl.transform(x_test)
                y_pred = clf.predict(x_test_std)[0]
                y_proba = clf.predict_proba(x_test_std)[0]

                all_pred.append(y_pred)
                all_proba_0.append(y_proba[0])
                all_proba_1.append(y_proba[1])
                ind += 1

            # Apply decision tree for age
            tree_model_age = self.models[-1]
            tree_clf = tree_model_age[1]
            age_test = df_curr.loc[:, 'Age'].values[0]
            age_test = np.array([age_test], dtype=np.float32)
            age_test = age_test.reshape(-1, 1)

            tree_pred = tree_clf.predict(age_test)
            tree_proba = tree_clf.predict_proba(age_test)
            all_pred.append(tree_pred[0])
            all_proba_0.append(tree_proba[0][0])
            all_proba_1.append(tree_proba[0][1])

            all_proba_0 = np.array(all_proba_0)
            all_proba_1 = np.array(all_proba_1)

            mean_proba_0 = np.mean(all_proba_0)
            mean_proba_1 = np.mean(all_proba_1)

            # dat = [mrn] + [tk] + [y_cad] + list(all_proba_1) + [mean_proba_1] + [mean_pp]
            # df_proba.loc[len(df_proba)] = dat

            if mean_proba_1 > self.proba_threshold:
                global_pred.append((mrn, 1))
                pred_data.append(1)
            else:
                global_pred.append((mrn, -1))
                pred_data.append(-1)

            mrn_proba_0.append(np.round(mean_proba_0, 4))
            mrn_proba_1.append(np.round(mean_proba_1, 4))

        # save results in csv file
        df = pd.DataFrame({'MRN': mrn_valid, 'Age': pat_age, 'TestKey': t_keys, 'CAD': cad_truth,
                           'Pred': pred_data, 'Proba': mrn_proba_1})
        n_tested = len(df)

        # Modify result removing cases close to decision probability
        df_tmp = df.loc[(df['Proba'] > 0.64) & (df['Proba'] < 0.66)]
        mrn_exc = df_tmp.MRN.values
        tk_exc = df_tmp.TestKey.values
        proba_exc = df_tmp.Proba.values
        if verbose:
            print('Excluded patients')
            for ii in range(len(mrn_exc)):
                print('MRN:', mrn_exc[ii], 'TK:', tk_exc[ii], 'Proba:', proba_exc[ii], 'TH:',
                      self.proba_threshold)

        # updated results
        df = df.loc[~df['MRN'].isin(mrn_exc)]

        cad_truth = df.CAD.values
        pred = df.Pred.values
        proba = df.Proba.values
        if verbose:
            print('Number of undetermined:', n_tested - len(df), 'out of:', n_tested)

        # label excluded patients with prediction class 0 for uncertain
        df_tmp.loc[:, 'Pred'] = 0
        df = pd.concat([df, df_tmp]).sort_index()
        # ==================== Done with patient/testKey processing ======================
        # classifier metrics
        if show_metrics:
            self.get_metrics(cad_truth, pred, proba)
        return df

    def get_metrics(self, y_test, y_pred, y_proba):
        # # ROC curve
        fpr, tpr, thresholds = roc_curve(y_test, y_proba)
        spec = 1 - np.array(fpr, dtype=np.float32)
        df_roc = pd.DataFrame({'TPR': tpr, 'FPR': fpr, 'SP': spec, 'THRESH': thresholds})
        # for ix, row in df_roc.iterrows():
        #     print('TPR', np.round(row['TPR'], 4), 'SP:', np.round(row['SP'], 4), 'TH:', np.round(row['THRESH'], 4))

        auc = np.round(roc_auc_score(y_test, y_proba), 3)
        print('AUC:', auc)

        plt.figure(figsize=(10, 8))
        plt.plot(fpr, tpr, label='AUC: ' + str(auc))
        plt.plot([0, 1], [0, 1], '--r')
        plt.xlabel('False positive rate (1 - Sp)')
        plt.ylabel('True positive rate (Se)')
        plt.title('ROC curve of SVM classifier')
        plt.legend(loc='lower right')
        plt.grid(True)
        # plt.show()

        # # Precision-recall curve
        # plt.figure(figsize=(10, 8))
        # prec, recalls, thresh = precision_recall_curve(y_test, y_proba)
        # plt.plot(recalls, prec, '-+')
        # plt.xlabel('Recall (Se)')
        # plt.ylabel('Precision (PPV)')
        # plt.title('Precision-recall curve of SVM classifier')
        # plt.grid(True)

        acc = accuracy_score(y_test, y_pred)
        acc = np.round(acc, 3)
        print('Acc:', acc)
        c_mat = confusion_matrix(y_true=y_test, y_pred=y_pred)
        tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
        self.metrics = [auc, acc, tn, fp, fn, tp]

        print('Confusion matrix:')
        print(c_mat)
        se = tp / (tp + fn)
        sp = tn / (tn + fp)
        ppv = tp / (tp + fp)
        npv = tn / (tn + fn)
        f1 = 2 / (1 / ppv + 1 / se)
        self.perf = [np.round(se, 3), np.round(sp, 3), np.round(ppv, 3), np.round(npv, 3), np.round(f1, 3)]

        print('TP =', tp, 'TN = ', tn, 'FP = ', fp, 'FN = ', fn)
        print('Se = ', np.round(se, 3))
        print('Sp = ', np.round(sp, 3))
        print('PPV = ', np.round(ppv, 3))
        print('NPV = ', np.round(npv, 3))
        print('F1 = ', np.round(f1, 3))

        plt.figure(figsize=(8, 6))
        sns.heatmap(c_mat, annot=True)
        plt.xlabel('Prediction')
        plt.ylabel('Truth labels')
        plt.show()

    def save_test(self, df_res, fname):
        """
        Test results
        :param df_res: dataframe with results
        :param filename: csv file name
        :return: Save data in file
        """
        df_res.to_csv(fname, index=False)

    def patient_test(self, df_pat):
        """ Test a new patient """
        ch_avail = df_pat.CH.values
        # check if patient has data channels for trained model
        for ch in self.channels:
            if ch not in ch_avail:
                raise ValueError('Error! Patient does not have required channels')
            else:
                pass

        all_pred = []
        all_proba_0 = []  # mean probability of class -1, negative
        all_proba_1 = []
        ind = 0
        # run predictions for specified channels
        for ch in self.channels:
            model = self.models[ind]
            scl = model[0]
            clf = model[1]

            row = df_pat.loc[df_pat['CH'] == ch]
            x_test = row.loc[:, self.columns].values
            x_test = x_test ** 2
            x_test_std = scl.transform(x_test)
            y_pred = clf.predict(x_test_std)[0]
            y_proba = clf.predict_proba(x_test_std)[0]

            all_pred.append(y_pred)
            all_proba_0.append(y_proba[0])
            all_proba_1.append(y_proba[1])
            ind += 1

        tree_model_age = self.models[-1]
        tree_clf_1 = tree_model_age[1]
        x_age = df_pat.loc[:, 'Age'].values[0]
        x_age = np.array([x_age], dtype=np.float32)
        x_age = x_age[:, np.newaxis]
        tree_1_pred = tree_clf_1.predict(x_age)
        tree_1_proba = tree_clf_1.predict_proba(x_age)
        all_pred.append(tree_1_pred[0])
        all_proba_0.append(tree_1_proba[0][0])
        all_proba_1.append(tree_1_proba[0][1])

        all_proba_0 = np.array(all_proba_0)
        all_proba_1 = np.array(all_proba_1)

        # print('MRN:', pid, 'CH:', 7, 'Y_True:', y_cad, 'Y-pred:', tree_pred[0], 'Proba:',
        #       np.round(tree_proba[0], 3))
        # print('|-----------------------------------------------------------|')

        # global_pred = np.array(all_pred, dtype='int')

        mean_proba_0 = np.mean(all_proba_0)
        mean_proba_1 = np.mean(all_proba_1)

        # soft voting using probabilities
        if (mean_proba_1 > 0.64) & (mean_proba_1 < 0.66):
            global_pred = 0
        else:
            if mean_proba_1 >= 0.66:
                global_pred = 1
            else:
                global_pred = -1

        mrn_proba_1 = mean_proba_1
        return global_pred, np.round(mrn_proba_1, 3)
