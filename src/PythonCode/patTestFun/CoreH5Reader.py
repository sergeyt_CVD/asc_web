"""
@description: CoreH5 reader class.
@author: Robin Castelino

"""

import os
import warnings

import pandas as pd
import h5py


class CoreH5Reader:
    """
    HDF5 reader for CAD-det data files
    """
    def __init__(self, file_full_path):
        """
        Constructor of class CoreH5Reader

        Parameters
        ----------
        file_full_path : str
            Path to HDF5 file on disk, or file-like object
        """
        self._coreh5_path = file_full_path
        self._f = None
        self._group_info = None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __repr__(self):
        if self._f:
            return 'coreH5v{} @ {}'.format(self.version, self._coreh5_path)
        else:
            return 'coreH5 <not connected>'

    def open(self):
        # connect to HDF5 file
        self._f = h5py.File(self._coreh5_path, 'r')
        # create group_info df
        self._group_info = pd.DataFrame(list(self._f.keys()), columns=['group_name'])
        self._group_info[['study', 'mrn', 'testkey']] = self._group_info['group_name'].str.split(pat='_', expand=True)

    def close(self):
        self._f.close()

    def get_attributes(self, obj_path, att_name=None):
        """
        Probes obj_name's attributes and returns attribute

        Parameters
        ----------
        obj_path : list
            Path to group or dataset.
            obj_path must be a list with path to object, for example:
                For /group1, obj_path = ['group1']
                For /group/dataset, obj_path = ['group', dataset']
        att_name : str or None
            If att_name is None, all attributes associated with the object are return as a dict
            Attribute name
        Returns
        -------
        out : attribute as stored in HDF5 file or dict of attributes if att_name=None

        """
        if isinstance(obj_path, list):
            for x in obj_path:
                if not isinstance(x, str):
                    raise ValueError('elements in list must be strings')
        else:
            raise ValueError('obj_name must be list of strings or str')

        # join list into unix path
        obj_path = '/'.join(obj_path)

        if att_name:
            return self._f[obj_path].attrs[att_name]
        else:
            return dict((k, v) for k, v in self._f[obj_path].attrs.items())

    @property
    def groups(self):
        """
        List all groups in HDF5 file

        Returns
        -------
        out : list

        """
        return self._group_info.group_name.tolist()

    def group_name(self, testkey):
        """
        Get group name using testkey

        Parameters
        ----------
        testkey : UUID str
            A single testkey as UUID str
        Returns
        -------
        out : str
            Group name of the specified testkey

        """

        if not isinstance(testkey, str):
            raise ValueError('testkey must be str')

        df = self._group_info[self._group_info.testkey == testkey]
        assert (len(df) == 1), "testKey returns more than one test"

        return df.group_name.iloc[0]

    def testkey(self, group_name=None, mrn=None, study=None, which='all'):
        """
        Returns the testkey or a list of testkeys from a specific group_name or MRN and Study
        If group_name is provided all other arguments are ignored.
        If mrn and study must be provided together.  If they are provided and more than one testkey
        exists of the specified pair, the 'which' argument can be used to return only one testkey
        (sorted by datetime).

        Parameters
        ----------
        group_name: str, optional
            Group name as listed in coreH5
        mrn : str, optional
            MRN of participant from Study
        study : str, optional
            Study name
        which : str, optional
            Can be:
            'first' - earliest testkey by datetime is returned
            'last' - latest testkey by datetime is returned
            'all' - if more than one testkey exists, all are returned

        Returns
        -------
        testkey : UUID str (if only one exists) or list of UUID strings

        Raises
        ------
        ValueError
            If testkey cannot be found
        """

        # inspect arguments
        if all(e is None for e in [group_name, mrn, study]):
            raise ValueError('group_name or (mrn, study) must be provided')

        if group_name is None:
            if mrn is None or study is None:
                raise ValueError('Both mrn and study must be provided, if group_name is not provided')
            else:
                if not (isinstance(mrn, str) or isinstance(study, str)):
                    raise ValueError('mrn and study must be strings')
        else:
            if not isinstance(group_name, str):
                raise ValueError('group_name argument must be a string')

        if group_name:
            return self._group_info[self._group_info.group_name == group_name].testkey.iloc[0]
        else:
            df = self._group_info[(self._group_info.mrn == mrn) & (self._group_info.study == study)].copy()

            if df.empty:
                raise ValueError("testkey cannot be found")

            df['date_and_time'] = df.group_name.apply(lambda x: self.get_attributes([x], 'caddet:date_and_time'))
            # df['date_and_time'] = df.group_name.apply(self.get_attribute, att_name='date_and_time')
            df['date_and_time'] = pd.to_datetime(df['date_and_time'], format='%Y-%m-%d %H:%M:%S')
            df.sort_values('date_and_time', inplace=True)

            if (df['testkey'].nunique() > 1) and (which != 'all'):
                if len(df) > 2:
                    warnings.warn('More than 2 testkeys were found; two are generally expected')
                if which == 'first':
                    df = df.iloc[[0]]
                elif which == 'last':
                    df = df.iloc[[-1]]

            testkey = df.testkey.tolist()

            if 1 == len(testkey):
                return testkey[0]
            else:
                return testkey

    def bhs(self, testkey, normalize=False):
        """
        Retrieves breath-hold datasets from a specified HDF5 group using testkey.

        Parameters
        ----------
        testkey : str
            A single testkey as UUID str
        normalize : bool
            Returns ndarrays normalized between [-1, 1] as float64

        Returns
        -------
        bhs : dict of ndarrays
            dict keys are dataset names
            dict values are ndarrays

        """

        if not isinstance(testkey, str):
            raise ValueError('testkey must be a str')

        df = self._group_info[self._group_info.testkey.isin([testkey])]

        if df.empty:
            raise ValueError("testkey does not exist")

        assert (len(df) == 1), "testKey returns more than one test"

        group = df.group_name.iloc[0]
        bh_names = self._f[group].keys()

        bit_depth = self.get_attributes([group], 'caddet:rm_adc_bit_depth')
        norm_factor = 2**(bit_depth-1)

        bhs = dict()
        for bh_name in bh_names:
            bh = self._f[group][bh_name][()]
            if normalize:
                bh = bh/norm_factor
            bhs[bh_name] = bh

        return bhs

    @property
    def path(self):
        return os.path.dirname(self._coreh5_path)

    @property
    def filename(self):
        return os.path.basename(self._coreh5_path)

    @property
    def version(self):
        return self.get_attributes(['/'], 'version')

    @property
    def last_updated(self):
        return self.get_attributes(['/'], 'last_updated')
