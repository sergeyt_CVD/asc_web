#!/usr/bin/python3
import sys
import cvdanalyze as cvd
from pathlib import Path
import os
import numpy as np
import wavio
from datetime import datetime
import h5py
import logging
import yaml
import pickle
tmp_dir = Path(__file__).parent.parent
sys.path.append(str(tmp_dir) + '/PythonCode/patTestFun')
from data_prep import data_prep
import time
import argparse
import json

VERSION = '0.1.0'


def to_coreH5(h5_path, test, tmiFileLocation):
    """
    Writes a caddet test to an h5 file

    Parameters
    ----------
    h5_path : str
        Full path of an h5 file. If no file exists it will be created

    test : cvdanalyze.Test
        Test object that will be written to the h5 file

    tmiFileLocation : str
        Directory containing the gain and recording files associated with 'test'

    """

    if not isinstance(test, cvd.Test):
        raise TypeError("test must be a cvdanalyze test object")

    if not isinstance(h5_path, str):
        raise TypeError("h5_path must be string")

    if not isinstance(h5_path, str):
        raise TypeError("tmiFileLocation must be string")

    if not os.path.isdir(tmiFileLocation):
        raise LookupError("tmiFileLocation directory not found")

    with h5py.File(h5_path, mode='a') as h5:
        h5.attrs['version'] = VERSION
        h5.attrs['last_updated'] = datetime.now().strftime("%Y-%m-%d, %H:%M:%S")
        group_name = '{}_{}_{}'.format(test.study_name, test.mrn, test.testkey)

        if group_name not in h5.keys():
            # get Gain and recording files
            files = test.files(abs_filepath=True)
            files = files[files['fileName'].str.contains('Gain|recording') == True]

            if not files.empty:

                g = h5.create_group(group_name)

                bh_counter = 0
                for idx, f in files.iterrows():
                    try:
                        tmi = wavio.read('{}/{}'.format(tmiFileLocation, os.path.basename(f.fileName)))
                    except Exception as e:
                        print('ERROR --- ', e)
                        logging.error('Exception occurred. Message {}'.format(e))
                    else:
                        assert test.rm_adc_bit_depth / 8 == tmi.sampwidth
                        assert test.rm_adc_sampling_rate == tmi.rate

                        bhs = test.breath_hold_indices(filekey=f.fileKey)
                        for _, bh in bhs.iterrows():
                            data = tmi.data[bh.startIndex:bh.stopIndex]
                            d = g.create_dataset('bh' + str(bh_counter), data=data, dtype=np.int32)
                            d.attrs['caddet:filekey'] = bh.fileKey
                            d.attrs['caddet:filename'] = os.path.basename(f.fileName)
                            d.attrs['caddet:segment'] = bh.segmentKey
                            d.attrs['caddet:startIndex'] = bh.startIndex
                            d.attrs['caddet:stopIndex'] = bh.stopIndex
                            d.attrs['caddet:classifierKey'] = bh.classifierKey
                            bh_counter += 1

                if not bh_counter:
                    print('Warning --- {} has no BHs - skipping group'.format(group_name))
                    logging.warning('{} has no BHs - skipping group'.format(group_name))
                    del h5[group_name]
                else:

                    if bh_counter < 3:
                        print('Warning < 3 Bhs for ', group_name)
                        logging.warning('{} has < 3 Bhs'.format(group_name))

                    # Log messages
                    # print('Added to HDF5: GroupName = {}; BHs = {}; '
                    #       'TmiFileLocation = {}'.format(group_name, bh_counter, tmiFileLocation))
                    logging.info('Added to HDF5: GroupName = {}; BHs = {};'
                                 'TmiFileLocation = {}'.format(group_name, bh_counter, tmiFileLocation))

                    # add group attributes
                    g.attrs['caddet:mrn'] = test.mrn
                    g.attrs['caddet:gender'] = test.patient_gender
                    g.attrs['caddet:fs'] = test.rm_adc_sampling_rate
                    g.attrs['caddet:num_of_bhs'] = bh_counter
                    g.attrs['caddet:filenames'] = list(files.fileName.apply(lambda x: os.path.basename(x)))
                    g.attrs['caddet:dob'] = test.patient_dob
                    g.attrs['caddet:dock_id'] = test.dock_id
                    g.attrs['caddet:sw_version'] = test.software_version
                    g.attrs['caddet:fw_version'] = test.rm_firmware_version
                    g.attrs['caddet:rm_fpga_analog_gain'] = test.rm_fpga_analog_gain
                    g.attrs['caddet:rm_adc_bit_depth'] = test.rm_adc_bit_depth
                    g.attrs['caddet:date_and_time'] = str(test.date_and_time)
                    g.attrs['caddet:exam_room'] = test.exam_room
                    g.attrs['caddet:location'] = test.location
                    g.attrs['caddet:study'] = test.study_name
                    g.attrs['caddet:operator-firstName'] = test.operator_firstname
                    g.attrs['caddet:operator-lastName'] = test.operator_lastname
                    g.attrs['caddet:operator-userName'] = test.operator_username
                    g.attrs['caddet:testkey'] = test.testkey

            else:
                print('Warning --- {} has no BHs - skipping group'.format(group_name))
                logging.warning('{} has no BHs - skipping group'.format(group_name))
        else:
            print('GroupName {} already exists in HDF5 file'.format(group_name))
            logging.info('GroupName {} already exists in HDF5 file'.format(group_name))


def convert_caddet_to_h5(db_path, output_folder=None, testkey=None, mrn=None, study=None):
    with cvd.DataBase(os.path.join(db_path)) as db:
        assert db.version == 4, 'DB version must be 4'

        if testkey:
            if not isinstance(testkey, list):
                tests = [testkey]
            else:
                tests = testkey
        elif mrn:
            df_tests = db.testkeys(study=study, have_acoustics=False, mrn=mrn)
            tests = df_tests.testKey.to_list()
        else:
            df_tests = db.testkeys(study=study, have_acoustics=False)
            tests = df_tests.testKey.to_list()

        for tk in tests:
            test = cvd.Test(db=db, testkey=tk)
            h5_filename = os.path.join(str(output_folder or ''), '{}_{}.h5'.format(test.mrn, test.testkey))
            to_coreH5(h5_filename, test, os.path.dirname(db_path))


def patientTest(svm_file, mrn, testKey, age, sex, h5_dir):
    """ Main code of classifier """
    PROJ_DIR = Path(__file__).parent.parent
    config_file = str(PROJ_DIR) + '/PythonCode/patTestFun/Config/config.yml'

    with open(config_file, 'r') as fl:
        config_dict = yaml.safe_load(fl)

    # h5_file = os.environ['OHI_DIR'] + 'PatientTestUpload/' + mrn + '_' + testKey + '.h5'
    h5_file = h5_dir + '/' + mrn + '_' + testKey + '.h5'

    chan2use = [1, 4, 5, 6]
    df_features = data_prep(mrn, h5_file, testKey, age, sex,
                            config_dict['STUDIES'], chan2use)

    model = pickle.load(open(svm_file, 'rb'))

    cad_pred, proba_1 = model.patient_test(df_features)

    if cad_pred == 0:
        cad = 'Undetermined'
        comment = 'Need more testing'
    else:
        if cad_pred > 0:
            cad = 'Positive'
            comment = 'CAD is likely'
        else:
            cad = 'Negative'
            comment = 'CAD is unlikely'

    cad_result = cad
    cad_proba = np.round(proba_1, 3)
    cad_proba = cad_proba - 0.65        # value of proba over threshold
    print('Done')
    return mrn, age, sex, testKey, cad_result, cad_proba, comment


if __name__ == '__main__':
    argv = sys.argv[1:]
    mrn = argv[0]
    age = argv[1]
    sex = argv[2]
    testKey = argv[3]
    dir_rec = argv[4]  # full path to data.db and file itself
    data_db = dir_rec + '/data.db'
    dir_out = argv[5]

    # generate h5 for selected mrn testKey combination
    convert_caddet_to_h5(data_db, output_folder=dir_out, mrn=mrn, testkey=testKey)

    # print('Done with h5')
    PROJ_DIR = Path(__file__).parent.parent
    model_file = str(PROJ_DIR) + '/PythonCode/patTestFun/Model/SVM_Model.sav'
    mrn, age, sex, testKey, test_result, proba, comments = patientTest(model_file, mrn, testKey, age, sex, dir_out)

#     res = {"mrn": mrn, "age": age, "sex": sex, "testKey": testKey,
#     		"result": test_result, "proba": proba, "comment": comment}
    print(mrn, age, sex, testKey, test_result, proba, comments)
#     res = json.dumps(res)
#     print(res)


